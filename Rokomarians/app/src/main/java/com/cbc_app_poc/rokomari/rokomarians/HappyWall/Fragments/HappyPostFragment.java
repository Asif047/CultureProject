package com.cbc_app_poc.rokomari.rokomarians.HappyWall.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Requests.HappyPostRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.squareup.picasso.Picasso;

import static android.content.Context.MODE_PRIVATE;


public class HappyPostFragment extends Fragment {

    private EditText etHappyPost;
    private ImageView ivProfile;
    private Button btnHappyPost;
    private TextView tvName;

    private HappyPostRequest happyPostRequest;
    private String url = BaseUrl.BASE_URL_APP+"happy-post/new-post";
    private String account_id = "", user_name="", user_image="";
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_happy_post, container, false);


        //getting account id starts
        SharedPreferences prefs = getContext().getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        String restoredName = prefs.getString("user_name", null);

        if (restoredName != null) {
            user_name = prefs.getString("user_name", "No account defined");
        }


        String restoredImage = prefs.getString("user_image", null);

        if (restoredImage != null) {
            user_image = prefs.getString("user_image", "No account defined");
        }
        //getting account id ends

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(getContext());

        happyPostRequest = new HappyPostRequest(getContext());

        etHappyPost = view.findViewById(R.id.edittext_happy_post);
        ivProfile = view.findViewById(R.id.imageview_profile);
        btnHappyPost = view.findViewById(R.id.button_add_happy_post);
        tvName = view.findViewById(R.id.textview_name_happy_post);


        etHappyPost.setFilters(new InputFilter[]{EMOJI_FILTER});


        tvName.setText(user_name);
        Picasso.with(getContext()).load(user_image).into(ivProfile);

        btnHappyPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!myNetworkCheck.isConnected(getContext())){
                    showAlert.showWarningNetHappyPostActivity();
                } else {

                    if(etHappyPost.getText().toString().isEmpty()) {
                        etHappyPost.setError("Please write something");
                    } else {
                        happyPostRequest.postData(etHappyPost.getText().toString(), url, account_id);
                    }

                }
            }
        });

        return view;

    }



    public static InputFilter EMOJI_FILTER = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE) {
                    return "";
                }
            }
            return null;
        }
    };


}
