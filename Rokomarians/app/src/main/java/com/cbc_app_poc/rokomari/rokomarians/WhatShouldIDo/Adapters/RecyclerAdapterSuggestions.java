package com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Adapters;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelSuggestion;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Requests.DeleteMySuggestion;

import java.util.List;

public class RecyclerAdapterSuggestions extends RecyclerView.Adapter<RecyclerAdapterSuggestions.MyViewHolder> {

    private List<ModelSuggestion> modelSuggestions;
    private Context context;
    private String id = "";
    private String status = "";

    private String account_id = "", user_id = "";

    private String problemId = "", suggestionId = "", path ="";

    DeleteMySuggestion deleteMySuggestion;

    public RecyclerAdapterSuggestions( Context context, List<ModelSuggestion> modelSuggestions,
                                       String account_id, String user_id) {


        this.context = context;
        this.modelSuggestions = modelSuggestions;
        this.account_id = account_id;
        this.user_id = user_id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_suggestions,
                                            parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        deleteMySuggestion = new DeleteMySuggestion(context);
        path = BaseUrl.BASE_URL_APP+"suggestion/ ";

        holder.tvSuggestions.setText(modelSuggestions.get(position).getSuggestion());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                problemId = modelSuggestions.get(pos).getProblemId();
                suggestionId = modelSuggestions.get(pos).getId();

                if (user_id.equals(modelSuggestions.get(pos).getUserId())) {
                    new AlertDialog.Builder(context)
                            .setMessage("Are you sure you want to delete?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    //finish();
                                    deleteMySuggestion.deleteData(problemId, suggestionId,
                                            account_id, path);

                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return modelSuggestions.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        TextView tvSuggestions;


        public MyViewHolder(View itemView) {
            super(itemView);

            tvSuggestions = itemView.findViewById(R.id.textview_suggestion);
        }


        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }


}
