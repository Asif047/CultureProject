package com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelProblem;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.SuggestionsActivity;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.SuggestionsMyProblemActivity;

import java.util.List;

public class RecyclerAdapterMyProblems extends RecyclerView.Adapter<RecyclerAdapterMyProblems.MyViewHolder>{


    private List<ModelProblem> modelProblems;
    private Context context;
    private String id = "";
    private String status = "";


    public RecyclerAdapterMyProblems(Context context, List<ModelProblem> modelProblems) {
        this.modelProblems = modelProblems;
        this.context = context;
    }

    @Override
    public RecyclerAdapterMyProblems.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_problems,
                parent, false);
        return new RecyclerAdapterMyProblems.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapterMyProblems.MyViewHolder holder, final int position) {

        holder.tvProblem.setText(modelProblems.get(position).getProblem());
        holder.tvProblemNum.setText(""+ (position+1));

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Intent intent = new Intent(context, SuggestionsMyProblemActivity.class);
                intent.putExtra("problem_num", ""+(pos+1));
                intent.putExtra("problem", ""+(modelProblems.get(pos).getProblem()));
                intent.putExtra("problem_id", ""+(modelProblems.get(pos).getId()));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelProblems.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        TextView tvProblem, tvProblemNum;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvProblem = itemView.findViewById(R.id.textview_problem);
            tvProblemNum = itemView.findViewById(R.id.textview_problem_num);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }


}
