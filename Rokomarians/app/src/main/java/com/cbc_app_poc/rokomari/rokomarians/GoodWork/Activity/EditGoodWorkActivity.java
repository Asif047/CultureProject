package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request.EditGoodWorkRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

public class EditGoodWorkActivity extends AppCompatActivity {

    private String nominated_id = "", nominated_name = "", nominated_image = "", nominated_reason="";

    private ImageView ivNominated;
    private TextView tvNominatedName;
    private EditText etNominatedReason;
    private Button btnNominate;

    private EditGoodWorkRequest editGoodWorkRequest;

    private String account_id = null, path="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_good_work);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends


        final Intent intent = getIntent();
        nominated_id = intent.getStringExtra("nominated_id");
        nominated_name = intent.getStringExtra("nominated_name");
        nominated_image = intent.getStringExtra("nominated_image");
        nominated_reason = intent.getStringExtra("nominated_reason");

       // Toast.makeText(EditGoodWorkActivity.this, nominated_id, Toast.LENGTH_LONG).show();


        ivNominated = findViewById(R.id.imageview_edit_post_nominated);
        tvNominatedName = findViewById(R.id.textview_edit_nominated_name);
        etNominatedReason = findViewById(R.id.edittext_edit_reason_nomination);
        btnNominate = findViewById(R.id.button_edit_add_nomination);

        Picasso.with(this).load(nominated_image).into(ivNominated);
        tvNominatedName.setText(nominated_name);
        etNominatedReason.setText(nominated_reason);


        path = BaseUrl.BASE_URL_APP +"nomination/"+nominated_id;

        editGoodWorkRequest = new EditGoodWorkRequest(this);

        btnNominate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editGoodWorkRequest.putData(path, etNominatedReason.getText().toString(), account_id);
            }
        });

    }
}
