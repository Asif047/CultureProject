package com.cbc_app_poc.rokomari.rokomarians.MeetMe.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.ApiCalls.ApiCallMemberList;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelMember;
import com.cbc_app_poc.rokomari.rokomarians.Model.User;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

import static android.content.Context.MODE_PRIVATE;


public class TeamFragment extends Fragment {


    private ImageView ivTeam;
    private TextView tvNameTeam, tvTeam, tvWorkTeam;



    private SweetAlertDialog pDialog;
    private String path, response;
    private OkHttpClient client;
    private ModelMember user;
    private ApiCallMemberList apiCallMemberList;
    List<User> data ;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private String account_id = null;
    private String member_id = "";




    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TeamFragment() {

    }

    public static TeamFragment newInstance(String param1, String param2) {
        TeamFragment fragment = new TeamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_team, container, false);


        //getting account id starts
        SharedPreferences prefs = getContext().getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        //getting account id ends

        final Intent intent = getActivity().getIntent();
        member_id = intent.getStringExtra("member_id");

        ivTeam = view.findViewById(R.id.image_team);
        tvNameTeam = view.findViewById(R.id.textview_name_team);
        tvTeam = view.findViewById(R.id.textview_team);
        tvWorkTeam = view.findViewById(R.id.textview_work_responsibility);


        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        apiCallMemberList = new ApiCallMemberList();
        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(getContext());


        try {
            path = BaseUrl.BASE_URL_APP + "users/"+member_id;

            if(!myNetworkCheck.isConnected(getContext())) {
                showAlert.showWarningNetMeetMe();
            } else{
                new TeamFragment.GetDataFromServer().execute();
            }
        } catch (Exception e){

        }



        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //Toast.makeText(context,"Team fragment attached", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }




    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            tvNameTeam.setText(user.getFirstName());
            tvTeam.setText(user.getUserOfficeInfo().getTeam());
            tvWorkTeam.setText(user.getUserOfficeInfo().getWork());

            Picasso.with(getContext()).load(user.getImage().getImageUrl()).into(ivTeam);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                client = new OkHttpClient();
                response = apiCallMemberList.GET(client, path, account_id);
                Log.e("###GET_MEMBER_WHO: ", response);
                Gson gson = new Gson();
                Type type = new TypeToken<Collection<User>>() {

                }.getType();

                user = gson.fromJson(response, ModelMember.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }




}
