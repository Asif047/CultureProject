package com.cbc_app_poc.rokomari.rokomarians;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.ChangePasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Activity.HappyWallActivity;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.FeelingsActivity;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.GetFeelingsActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.IdeaBoxActivity;
import com.cbc_app_poc.rokomari.rokomarians.Journey.SplashJourney;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.MeetMeActivity;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.MemberListActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelHome;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelNoticeBoard;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelNotification;
import com.cbc_app_poc.rokomari.rokomarians.NoticeBoard.ApiCallNoticeBoard;
import com.cbc_app_poc.rokomari.rokomarians.Notification.ApiCallNotification;
import com.cbc_app_poc.rokomari.rokomarians.Profile.ProfileActivity;
import com.cbc_app_poc.rokomari.rokomarians.RecreationHour.MyRecordActivity;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.WhatActivity;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

public class HomeActivity extends AppCompatActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;

    private TextView tvJourney, tvIdea, tvGoodWork, tvRecreation, tvWhat, tvHappyWall, tvMeetMe,
            tvFeeling, tvNotification;
    private TextView tvNumIdea, tvNumGoodWork, tvNumHappyWall, tvNumRecreation, tvNumWhat;

    private CardView cardJourney, cardIdea, cardMeetMe, cardGoodWork, cardRecreation, cardHappyWall, cardWhat, cardFeelings;

    private String account_id = "", email = "", email_profile = "", first_name = "", last_name = "", image = "", notice="";

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    SweetAlertDialog pDialog;
    private String path, path_notice, path_notification, response;
    OkHttpClient client;
    private ModelHome modelHomes;
    private ModelNoticeBoard[] modelNoticeBoard;
    private ApiCallHome apiCallHome;
    private ApiCallNoticeBoard apiCallNoticeBoard;
    private ApiCallNotification apiCallNotification;
    private List<ModelNoticeBoard> data_notice;
    private ModelNotification modelNotification;

    NavigationView nvDrawer;

    private int notifi_happy = -1, notifi_idea = -1, notifi_good_work = -1, notifi_what = -1,
            notifi_journey = -1;

    private static final String BASE_URL_PROFILE ="http://192.168.11.231:9090/";

    private Tracker mTracker;

    private RelativeLayout relativeHappy, relativeIdea, relativeGoodWork, relativeWhat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        apiCallHome = new ApiCallHome();
        apiCallNoticeBoard = new ApiCallNoticeBoard();
        apiCallNotification = new ApiCallNotification();
        data_notice = new ArrayList<>();
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);
        String restoredEmail = prefs.getString("email", null);


        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        if (restoredEmail != null) {
            email = prefs.getString("email", "No email defined");
        }
        //getting account id ends



        //getting notify_happy starts
        SharedPreferences prefs2 = getSharedPreferences("Notify_PREF", MODE_PRIVATE);
        int restoredNotifyHappy = prefs2.getInt("notifi_happy", 0);

        if(restoredNotifyHappy != 0){
            notifi_happy = prefs2.getInt("notifi_happy", 0);
        }
        //getting notify_happy ends


        //getting notify_idea starts
        int restoredNotifyIdea = prefs2.getInt("notifi_idea", 0);

        if(restoredNotifyIdea != 0){
            notifi_idea = prefs2.getInt("notifi_idea", 0);
        }
        //getting notify_idea ends


        //getting notify_good_work starts
        int restoredNotifyGoodWork = prefs2.getInt("notifi_good_work", 0);

        if(restoredNotifyGoodWork != 0){
            notifi_good_work = prefs2.getInt("notifi_good_work", 0);
        }
        //getting notify_good_work ends



        //getting notify_good_work starts
        int restoredNotifyWhat = prefs2.getInt("notifi_what", 0);

        if(restoredNotifyWhat != 0){
            notifi_what = prefs2.getInt("notifi_what", 0);
        }
        //getting notify_good_work ends


       // Toast.makeText(HomeActivity.this, account_id, Toast.LENGTH_LONG).show();


        cardJourney = findViewById(R.id.card_journey);
        cardIdea = findViewById(R.id.card_idea);
        cardMeetMe = findViewById(R.id.card_meet_me);
        cardGoodWork = findViewById(R.id.card_good_work);
        cardRecreation = findViewById(R.id.card_recreation);
        cardHappyWall = findViewById(R.id.card_happy_wall);
        cardWhat = findViewById(R.id.card_what);
        cardFeelings = findViewById(R.id.card_feeling);

        tvJourney = findViewById(R.id.textview_journey);
        tvIdea = findViewById(R.id.textview_idea);
        tvGoodWork = findViewById(R.id.textview_good_work);
        tvRecreation = findViewById(R.id.textview_recreation);
        tvWhat = findViewById(R.id.textview_what_should_i_do);
        tvHappyWall = findViewById(R.id.textview_happy_wall);
        tvMeetMe = findViewById(R.id.textview_meet_me);
        tvFeeling = findViewById(R.id.textview_feeling);
        tvNotification = findViewById(R.id.textview_notification);

        tvNumIdea = findViewById(R.id.num_idea);
        tvNumGoodWork = findViewById(R.id.num_good_work);
        tvNumHappyWall = findViewById(R.id.num_happy_wall);
        tvNumRecreation = findViewById(R.id.num_recreation);
        tvNumWhat = findViewById(R.id.num_what);

        relativeHappy = findViewById(R.id.relative_happy);
        relativeGoodWork = findViewById(R.id.relative_good_work);
        relativeIdea = findViewById(R.id.relative_idea);
        relativeWhat = findViewById(R.id.relative_what);

        Typeface tf = Typeface.createFromAsset(getAssets(), "font_amaranth/Amaranth-Bold.ttf");
        tvJourney.setTypeface(tf);
        tvIdea.setTypeface(tf);
        tvGoodWork.setTypeface(tf);
        tvRecreation.setTypeface(tf);
        tvWhat.setTypeface(tf);
        tvHappyWall.setTypeface(tf);
        tvMeetMe.setTypeface(tf);
        tvFeeling.setTypeface(tf);

        getSupportActionBar().hide();

        tvNotification.setText("* This is a test bulletin. This is a long news bulletin. Hope you all are well.............");
        tvNotification.setSelected(true);
        //get notification ends

        dl = (DrawerLayout) findViewById(R.id.dl);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        // Setting toolbar as the ActionBar with setSupportActionBar() call


        toolbar.setTitle("Home Page");
        toolbar.inflateMenu(R.menu.logout_menu);


        toggle = new ActionBarDrawerToggle(this, dl, toolbar, R.string.open, R.string.close);
        dl.addDrawerListener(toggle);

        nvDrawer = (NavigationView) findViewById(R.id.nv);
        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        setupDrawerContent(nvDrawer);


        try {
            path = BaseUrl.BASE_URL_APP + "logged-in-user/";
            path_notice = BaseUrl.BASE_URL_APP + "notice/";
            path_notification = BaseUrl.BASE_URL_APP + "notifications/";
            if(account_id != null){
                if(!myNetworkCheck.isConnected(this)){
                    showAlert.showWarningNetHomeActivity();
                } else{

                        new GetDataFromServer().execute();
                        new GetNoticeFromServer().execute();
                        new GetNotificationFromServer().execute();

                }

            }

        } catch (Exception e) {

        }


        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();


    }


    public void selectItemDrawer(MenuItem menuItem) {
        Fragment myFragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.home:
//                Toast.makeText(RememberMeActivity.this,"hello",Toast.LENGTH_LONG).show();
//                fragmentClass = Network.class;
                break;
            case R.id.profile:
                if (!myNetworkCheck.isConnected(HomeActivity.this)) {
                    showAlert.showWarningNetHomeActivity();
                } else {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                }
                break;

            case R.id.log_out_drawer:

                account_id = null;
                SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                editor.putString("account_id", account_id);
                editor.apply();

                Intent intent = new Intent(HomeActivity.this, LogInActivity.class);
                startActivity(intent);

                break;


            case R.id.change_password:

                Intent intent2 = new Intent(HomeActivity.this, ChangePasswordActivity.class);
                startActivity(intent2);

                break;

            case R.id.mood_meter :
                Intent intent3 = new Intent(HomeActivity.this, GetFeelingsActivity.class);
                startActivity(intent3);
                break;
            default:
                // fragmentClass = Network.class;
        }
        try {
//            myFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.flcontent,myFragment).commit();
        menuItem.setChecked(true);
        //setTitle(menuItem.getTitle());
        dl.closeDrawers();


    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItemDrawer(item);
                return true;
            }
        });
    }


    public void logoutClick(MenuItem item) {
        account_id = null;
        SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
        editor.putString("account_id", account_id);
        editor.apply();

        Intent intent = new Intent(HomeActivity.this, LogInActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout_menu, menu);

        return true;
    }


    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try{
                //new starts
                View hView = nvDrawer.getHeaderView(0);
                TextView nav_user_name = (TextView) hView.findViewById(R.id.textview_name_header);
                TextView nav_user_email = (TextView) hView.findViewById(R.id.textview_email_header);
                ImageView nav_user_image = (CircleImageView) hView.findViewById(R.id.image_header);

                nav_user_name.setText(modelHomes.getFirstName());
                nav_user_email.setText(modelHomes.getEmail());
                Picasso.with(HomeActivity.this).load(modelHomes.getImagePath()).into(nav_user_image);

                SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                editor.putString("user_id", ""+modelHomes.getId());
                editor.putString("user_name", modelHomes.getFirstName());
                editor.putString("user_email", modelHomes.getEmail());
                editor.putString("user_image", modelHomes.getImagePath());
                editor.apply();
                //new ends

                if(modelHomes.getId() == null){
                    Intent intent = new Intent(HomeActivity.this, LogInActivity.class);
                    startActivity(intent);
                }

               // Toast.makeText(HomeActivity.this, "http://192.168.11.231:9090/"+modelHomes.getImagePath(), Toast.LENGTH_LONG).show();
            } catch (Exception e){

            }


        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                if(account_id != null) {

                    client = new OkHttpClient();
                    response = apiCallHome.GET(client, path, email, account_id);
                    Log.e("##HOME_JSON: ", response);

                    if(response.trim().equals("invalid")) {
                        account_id = null;

                        SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                        editor.putString("account_id", account_id);
                        editor.putString("email", email);
                        editor.apply();

                        finish();

                    } else {
                        Gson gson = new Gson();
                        modelHomes = gson.fromJson(response, ModelHome.class);
                    }


                }




            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }
    }


    private class GetNoticeFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }


            for (int i = 0; i< data_notice.size(); i++){
                notice = notice+ "... " +data_notice.get(i).getNotice();
            }

            tvNotification.setText(notice);


            cardJourney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, SplashJourney.class);
                    startActivity(intent);
                }
            });

            cardIdea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{
                        SharedPreferences.Editor editor = getSharedPreferences("Notify_PREF", MODE_PRIVATE).edit();
                        editor.putInt("notifi_idea", modelNotification.getIdeas());
                        editor.apply();

                        Intent intent = new Intent(HomeActivity.this, IdeaBoxActivity.class);
                        startActivity(intent);
                    } catch (Exception e){

                    }

                }
            });

            cardMeetMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(HomeActivity.this, MemberListActivity.class);
                    startActivity(intent);
                }
            });

            cardGoodWork.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{
                        SharedPreferences.Editor editor = getSharedPreferences("Notify_PREF", MODE_PRIVATE).edit();
                        editor.putInt("notifi_good_work", modelNotification.getNominations());
                        editor.apply();

                        Intent intent = new Intent(HomeActivity.this, GoodWorkActivity.class);
                        startActivity(intent);
                    } catch (Exception e){

                    }

                }
            });

            cardRecreation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try{
                        Intent intent = new Intent(HomeActivity.this, MyRecordActivity.class);
                        startActivity(intent);
                    } catch (Exception e){

                    }

                }
            });

            cardHappyWall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                        SharedPreferences.Editor editor = getSharedPreferences("Notify_PREF", MODE_PRIVATE).edit();
                        editor.putInt("notifi_happy", modelNotification.getHappyPosts());
                        editor.apply();

                        Intent intent = new Intent(HomeActivity.this, HappyWallActivity.class);
                        startActivity(intent);
                    } catch (Exception e){

                    }


                }
            });

            cardWhat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, WhatActivity.class);
                    startActivity(intent);
                }
            });

            cardFeelings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, FeelingsActivity.class);
                    startActivity(intent);
                }
            });



        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {


                if(account_id != null) {


                    client = new OkHttpClient();

                    response = apiCallNoticeBoard.GET(client, path_notice, account_id);
                    Log.e("##GET_NOTICE: ", response);


                    if(response.trim().equals("invalid")) {
                        account_id = null;

                        SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                        editor.putString("account_id", account_id);
                        editor.putString("email", email);
                        editor.apply();

                        finish();

                    } else {


                        Gson gson = new Gson();
                        Type type = new TypeToken<Collection<ModelNoticeBoard>>() {

                        }.getType();

                        Collection<ModelNoticeBoard> enums = gson.fromJson(response, type);
                        modelNoticeBoard = enums.toArray(new ModelNoticeBoard[enums.size()]);

                        if(data_notice.isEmpty()) {
                            for (int i =0; i<enums.size(); i++){
                                data_notice.add(modelNoticeBoard[i]);
                            }
                        }


                    }


                }


            } catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }



    private class GetNotificationFromServer extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

           // tvNumGoodWork.setText(""+modelNotification.getNominations());
           // tvNumHappyWall.setText(""+modelNotification.getHappyPosts());
            //tvNumIdea.setText(""+modelNotification.getIdeas());


            try {

                if(notifi_happy == -1){
                    notifi_happy = modelNotification.getHappyPosts();

                } else if(modelNotification.getHappyPosts()>= notifi_happy){
                    notifi_happy = modelNotification.getHappyPosts() - notifi_happy;
                } else if(modelNotification.getHappyPosts()< notifi_happy) {
                    notifi_happy = 0;
//                    relativeHappy.setVisibility(View.GONE);
                }



                if(notifi_idea == -1){
                    notifi_idea = modelNotification.getIdeas();

                } else if(modelNotification.getIdeas()>= notifi_idea){
                    notifi_idea = modelNotification.getIdeas() - notifi_idea;
                } else if(modelNotification.getIdeas()< notifi_idea) {
                    notifi_idea = 0;
                }


                if(notifi_good_work == -1){
                    notifi_good_work = modelNotification.getNominations();

                } else if(modelNotification.getNominations()>= notifi_good_work){
                    notifi_good_work = modelNotification.getNominations() - notifi_good_work;
                } else if(modelNotification.getNominations()< notifi_good_work) {
                    notifi_good_work = 0;
                }


                if(notifi_happy <= 0) {
                    relativeHappy.setVisibility(View.GONE);
                }

                if(notifi_idea <= 0) {
                    relativeIdea.setVisibility(View.GONE);
                }

                if(notifi_good_work <= 0) {
                    relativeGoodWork.setVisibility(View.GONE);
                }

                tvNumHappyWall.setText(""+notifi_happy);
                tvNumIdea.setText(""+notifi_idea);
                tvNumGoodWork.setText(""+notifi_good_work);


            } catch (Exception e) {

            }




        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{

                if(account_id != null) {


                    client = new OkHttpClient();
                    response = apiCallNotification.GET(client, path_notification, account_id);
                    Log.e("##GET_NOTIFICATION: ", response);


                    if(response.trim().equals("invalid")) {
                        account_id = null;

                        SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                        editor.putString("account_id", account_id);
                        editor.putString("email", email);
                        editor.apply();

                        finish();

                    } else {

                        Gson gson = new Gson();
                        modelNotification = gson.fromJson(response, ModelNotification.class);


                    }





                }



            } catch (IOException e) {
                e.printStackTrace();
            }
           return null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Home Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

    //back button operation starts
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //finish();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    //back button operation ends


}
