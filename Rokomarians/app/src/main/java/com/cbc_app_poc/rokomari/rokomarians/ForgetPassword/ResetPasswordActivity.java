package com.cbc_app_poc.rokomari.rokomarians.ForgetPassword;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.ChangePasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.Requests.PostPasswordResetRequest;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;

public class ResetPasswordActivity extends AppCompatActivity {


    private EditText etResetPassword, etConfirmPassword;
    private Button btnConfirm;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private PostPasswordResetRequest postPasswordResetRequest;
    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        etResetPassword = findViewById(R.id.edittext_reset_new_password);
        etConfirmPassword = findViewById(R.id.edittext_reset_confirm_password);
        btnConfirm = findViewById(R.id.button_reset_confirm_password);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        path = BaseUrl.BASE_URL_APP + "forget_password/reset-password";

        postPasswordResetRequest = new PostPasswordResetRequest(this);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean passwordChecked = checkPassword();

                if(passwordChecked == true) {

                    if(!etResetPassword.getText().toString().isEmpty() &&
                            !etConfirmPassword.getText().toString().isEmpty() &&
                            (etResetPassword.getText().toString().equals(etConfirmPassword.
                                    getText().toString())) ) {

                        if(!myNetworkCheck.isConnected(ResetPasswordActivity.this)) {
                            showAlert.showWarningNetPasswordChange();
                        } else {

                            postPasswordResetRequest.postData(etConfirmPassword.getText().toString(),
                                    path);

                        }

                    }


                }



            }
        });



    }





    private boolean checkPassword() {



        if(etResetPassword.getText().toString().isEmpty()) {
            etResetPassword.setError("Please enter the password");
            return false;
        }

        if(etConfirmPassword.getText().toString().isEmpty()) {
            etConfirmPassword.setError("Please confirm the password");
            return false;
        }



        if(etResetPassword.getText().toString().length() < 6 || etResetPassword.getText()
                .toString().length()>32) {
            etResetPassword.setError("Character Limit should be 6 to 32");
            return false;
        }

        if(etConfirmPassword.getText().toString().length() < 6 || etConfirmPassword.getText()
                .toString().length()>32) {
            etConfirmPassword.setError("Character Limit should be 6 to 32");
            return false;
        }


        if(!etResetPassword.getText().toString().equals(etConfirmPassword.
                getText().toString()) ){

            Toast.makeText(ResetPasswordActivity.this, "New password & Confirmed Password " +
                    "didn't match", Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;

    }




    //back button operation starts

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(ResetPasswordActivity.this, ResetCodeActivity.class);
        startActivity(intent);
    }

    //back button operation ends




}
