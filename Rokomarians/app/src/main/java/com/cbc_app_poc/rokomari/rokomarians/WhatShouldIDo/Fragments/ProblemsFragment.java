package com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Adapter.RecyclerAdapterIdeas;
import com.cbc_app_poc.rokomari.rokomarians.Journey.Adapters.RecyclerAdapterJourney;
import com.cbc_app_poc.rokomari.rokomarians.Journey.ApiCalls.ApiCallJourney;
import com.cbc_app_poc.rokomari.rokomarians.Journey.JourneyActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelJourney;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelProblem;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Adapters.RecyclerAdapterProblems;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.ApiCalls.ApiCallProblems;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

import static android.content.Context.MODE_PRIVATE;


public class ProblemsFragment extends Fragment {

    private SweetAlertDialog pDialog;
    private String path, response, account_id= null;
    private OkHttpClient client;
    private ApiCallProblems apiCallProblems;
    private ModelProblem[] modelProblems;
    private List<ModelProblem> data;
    private RecyclerView recyclerView;
    private RecyclerAdapterProblems recyclerAdapterProblems;
    private RecyclerView.LayoutManager layoutManager;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //getting account id starts
        SharedPreferences prefs = getContext().getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_problems, container, false);

        apiCallProblems = new ApiCallProblems();
        data = new ArrayList<>();

        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(getContext());

        recyclerView = view.findViewById(R.id.recyclerview_problems);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        if(!myNetworkCheck.isConnected(getContext())) {
            showAlert.showWarningNetWhat();
        } else {
            try  {
                path= BaseUrl.BASE_URL_APP + "problems/ ";
                new GetDataFromServer().execute();
            } catch (Exception e){

            }
        }

        return view;
    }


    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterProblems = new RecyclerAdapterProblems(getContext(), data);
            recyclerView.setAdapter(recyclerAdapterProblems);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                client = new OkHttpClient();
                response = apiCallProblems.GET(client, path, account_id);
                Log.e("##GET_JOURNEY : ", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelProblem>>() {

                }.getType();

                Collection<ModelProblem> enums = gson.fromJson(response, type);
                modelProblems = enums.toArray(new ModelProblem[enums.size()]);

                if(data.isEmpty()) {
                    for( int i=0; i<enums.size(); i++) {
                        data.add(modelProblems[i]);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }
    }


}
