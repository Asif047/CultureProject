package com.cbc_app_poc.rokomari.rokomarians.MeetMe;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.Requests.EditCommentRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;

public class EditCommentActivity extends AppCompatActivity {

    private EditText etCommentEdit;
    private Button btnSave;

    private String comment_id = "", comment, account_id="", path = "";
    private EditCommentRequest editCommentRequest;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_comment);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        editCommentRequest = new EditCommentRequest(this);

        etCommentEdit = findViewById(R.id.edittext_say_hi_edit_comment);
        btnSave = findViewById(R.id.button_add_say_hi_edit_comment);

        final Intent intent = getIntent();
        comment_id = intent.getStringExtra("comment_id");
        comment = intent.getStringExtra("comment");

        path = BaseUrl.BASE_URL_APP + "say-hi/"+comment_id;


        etCommentEdit.setText(comment);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!myNetworkCheck.isConnected(EditCommentActivity.this)) {
                    showAlert.showWarningNetMeetMe();
                } else {
                    editCommentRequest.putData(path, etCommentEdit.getText().toString(),
                                                account_id);


                }

            }
        });



    }




    //back button operation starts
    @Override
    public void onBackPressed() {
        finish();
    }
    //back button operation ends



}
