package com.cbc_app_poc.rokomari.rokomarians.HappyWall.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Requests.DeleteHappyPostRequest;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.ApiCalls.ApiCallMyHappyPost;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelHappySeeAll;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import okhttp3.OkHttpClient;

public class DetailsMyHappyPostActivity extends AppCompatActivity {


    private SweetAlertDialog pDialog;
    private String path, response;
    private OkHttpClient client;
    private ModelHappySeeAll modelHappySeeAll;
    private ApiCallMyHappyPost apiCallMyHappyPost;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private String account_id = "", happy_post = "";
    private int post_id = 0;
    private TextView tvname, tvDetails;
    private ImageView ivProfile;
    private LinearLayout editLayout, deleteLayout;

    private DeleteHappyPostRequest deleteHappyPostRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_my_happy_post);

        final Intent intent = getIntent();
        post_id = intent.getIntExtra("happy_post_id", 0);

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        //getting account id ends

        apiCallMyHappyPost = new ApiCallMyHappyPost();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        tvname = findViewById(R.id.textview_my_happy_post_name);
        tvDetails = findViewById(R.id.textview_my_happy_post_details);
        ivProfile = findViewById(R.id.imageview_my_happy_post);

        editLayout = findViewById(R.id.edit_layout);
        deleteLayout = findViewById(R.id.delete_layout);


        editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsMyHappyPostActivity.this, UpdateHappyPostActivity.class);
                intent.putExtra("post_id", post_id);
                intent.putExtra("happy_post", modelHappySeeAll.getDetails());
                startActivity(intent);
            }
        });

        deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(DetailsMyHappyPostActivity.this)
                        .setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //finish();
                                deleteHappyPostRequest.deleteData(BaseUrl.BASE_URL_APP, account_id, post_id);

                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
        });



        deleteHappyPostRequest = new DeleteHappyPostRequest(this);

        try{
            path = BaseUrl.BASE_URL_APP + "happy-post/get-post";
            if(!myNetworkCheck.isConnected(this)){
                showAlert.showWarningNetDetailsHappyPostActivity();
            }
            else {
                new GetDataFromServer().execute();
            }

        } catch (Exception e){

        }

    }

    public void menuUpdateHappyPost(MenuItem item) {

        Intent intent = new Intent(this, UpdateHappyPostActivity.class);
        intent.putExtra("post_id", post_id);
        intent.putExtra("happy_post", modelHappySeeAll.getDetails());
        startActivity(intent);
    }

    public void menuDeleteHappyPost(MenuItem item) {

        deleteHappyPostRequest.deleteData(BaseUrl.BASE_URL_APP, account_id, post_id);
    }


    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try{
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()){
                pDialog.dismiss();
            }

            tvname.setText(modelHappySeeAll.getName());
            tvDetails.setText(modelHappySeeAll.getDetails());
            Picasso.with(DetailsMyHappyPostActivity.this).load(modelHappySeeAll.getImage()).into(ivProfile);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                client = new OkHttpClient();
                response = apiCallMyHappyPost.GET(client,path, account_id, post_id);
                Gson gson = new Gson();
                Log.e("#MY_POST:", response);
                modelHappySeeAll = gson.fromJson(response, ModelHappySeeAll.class);
            } catch (IOException e){
                e.printStackTrace();
            }

          return null;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.details_happy_post_menu, menu);
        return true;
    }


    //back button operation starts
    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(DetailsMyHappyPostActivity.this, HappyWallActivity.class);
//        startActivity(intent);
        finish();
    }
    //back button operation ends

}
