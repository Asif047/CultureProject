package com.cbc_app_poc.rokomari.rokomarians.Journey;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Adapter.RecyclerAdapterMyIdeas;
import com.cbc_app_poc.rokomari.rokomarians.Journey.Adapters.RecyclerAdapterJourney;
import com.cbc_app_poc.rokomari.rokomarians.Journey.ApiCalls.ApiCallJourney;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelJourney;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

public class JourneyActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog;
    private String path, response, account_id= null;
    private OkHttpClient client;
    private ApiCallJourney apiCallJourney;
    private ModelJourney[] modelJourneys;
    private List<ModelJourney> data;
    private RecyclerView recyclerView;
    private RecyclerAdapterJourney recyclerAdapterJourney;
    private RecyclerView.LayoutManager layoutManager;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends


        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        apiCallJourney = new ApiCallJourney();
        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);
        recyclerView = findViewById(R.id.recyclerview_journey);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        if(!myNetworkCheck.isConnected(this)) {
            showAlert.showWarningNetJourney();
        } else {
            try  {
                path = BaseUrl.BASE_URL_APP + "rokomarijourney/";
                new GetDataFromServer().execute();
            } catch (Exception e){

            }
        }

    }


    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterJourney = new RecyclerAdapterJourney(JourneyActivity.this, data);
            recyclerView.setAdapter(recyclerAdapterJourney);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                client = new OkHttpClient();
                response = apiCallJourney.GET(client, path, account_id);
                Log.e("##GET_JOURNEY : ", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelJourney>>() {

                }.getType();

                Collection<ModelJourney> enums = gson.fromJson(response, type);
                modelJourneys = enums.toArray(new ModelJourney[enums.size()]);

                if(data.isEmpty()) {
                    for( int i=0; i<enums.size(); i++) {
                        data.add(modelJourneys[i]);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }
    }

}
