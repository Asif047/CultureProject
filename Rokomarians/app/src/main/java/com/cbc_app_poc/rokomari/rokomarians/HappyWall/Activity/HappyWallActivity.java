package com.cbc_app_poc.rokomari.rokomarians.HappyWall.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.ChangePasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Adapter.SectionPageadapter;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Fragments.HappyPostFragment;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Fragments.MyHappyPostFragment;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Fragments.SeeAllFragment;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Fragments.WhatNewFragment;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.GetFeelingsActivity;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.Profile.ProfileActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class HappyWallActivity extends AppCompatActivity {

    private static final String TAG = "HappyWallActivity";
    private SectionPageadapter mSectionPageadapter;
    private ViewPager mViewPager;

    private Toolbar toolbar;

    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    NavigationView nvDrawer;

    private String account_id = null, user_email = "", user_name = "", user_image = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_wall);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        String restoredName = prefs.getString("user_name", null);

        if (restoredName != null) {
            user_name = prefs.getString("user_name", "No account defined");
        }

        String restoredEmail = prefs.getString("user_email", null);

        if (restoredEmail != null) {
            user_email = prefs.getString("user_email", "No account defined");
        }

        String restoredImage = prefs.getString("user_image", null);

        if (restoredImage != null) {
            user_image = prefs.getString("user_image", "No account defined");
        }


        //getting account id ends

        getSupportActionBar().hide();
        toolbar = (Toolbar) findViewById(R.id.toolbar_happy_wall);
        toolbar.setTitle("Happy Wall");

        Log.d(TAG, "on create:starting.");

        mSectionPageadapter = new SectionPageadapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        // navigation drawer work starts

        dl = (DrawerLayout) findViewById(R.id.dl);
        nvDrawer = (NavigationView) findViewById(R.id.nv);

        toggle = new ActionBarDrawerToggle(this, dl, toolbar, R.string.open, R.string.close);
        dl.addDrawerListener(toggle);

        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        setupDrawerContent(nvDrawer);

        // navigation drawer work ends


        //new starts
        View hView = nvDrawer.getHeaderView(0);
        TextView nav_user_name = (TextView) hView.findViewById(R.id.textview_name_header);
        TextView nav_user_email = (TextView) hView.findViewById(R.id.textview_email_header);
        ImageView nav_user_image = (CircleImageView) hView.findViewById(R.id.image_header);

        nav_user_name.setText(user_name);
        nav_user_email.setText(user_email);
        Picasso.with(this).load(user_image).into(nav_user_image);

        //new ends

    }


    private void setupViewPager(ViewPager viewPager) {
        SectionPageadapter sectionPageadapter = new SectionPageadapter(getSupportFragmentManager());
        sectionPageadapter.addFragment(new WhatNewFragment(), "What's New");
        sectionPageadapter.addFragment(new SeeAllFragment(), "See All");
        sectionPageadapter.addFragment(new HappyPostFragment(), "Write New");
        sectionPageadapter.addFragment(new MyHappyPostFragment(), "My Posts");

        viewPager.setAdapter(sectionPageadapter);

    }


    public void selectItemDrawer(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(HappyWallActivity.this, HomeActivity.class);
                startActivity(intent);
                break;
            case R.id.profile:
                Intent intent1 = new Intent(HappyWallActivity.this, ProfileActivity.class);
                startActivity(intent1);
                break;
            case R.id.log_out_drawer:

                account_id = null;
                SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                editor.putString("account_id", account_id);
                editor.apply();

                Intent intent2 = new Intent(HappyWallActivity.this, LogInActivity.class);
                startActivity(intent2);

                break;


            case R.id.change_password:

                Intent intent3 = new Intent(HappyWallActivity.this, ChangePasswordActivity.class);
                startActivity(intent3);

                break;

            case R.id.mood_meter :
                Intent intent4 = new Intent(HappyWallActivity.this, GetFeelingsActivity.class);
                startActivity(intent4);
                break;

            default:

        }

        menuItem.setChecked(true);
        //setTitle(menuItem.getTitle());
        dl.closeDrawers();


    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItemDrawer(item);
                return true;
            }
        });
    }


    //back button operation starts
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HappyWallActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
    //back button operation ends


}
