package com.cbc_app_poc.rokomari.rokomarians.MeetMe.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.Adapter.RecyclerAdapterSayHiComments;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.ApiCalls.ApiCallSayHi;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.Requests.PostSayHiRequest;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelSayHiComments;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

import static android.content.Context.MODE_PRIVATE;


public class SayHiFragment extends Fragment {


    private SweetAlertDialog pDialog;
    private String path, path_post_comment, response;
    private OkHttpClient client;
    private ModelSayHiComments[] modelSayHiComments;
    private ApiCallSayHi apiCallSayHi;
    private List<ModelSayHiComments> data;

    private RecyclerView recyclerView;
    private RecyclerAdapterSayHiComments recyclerAdapterSayHiComments;
    private RecyclerView.LayoutManager layoutManager;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;
    private String account_id = "";
    private String member_id = "", member_name="", member_image ="";

    private TextView tvUserName;
    private ImageView ivUser;
    private EditText etComment;
    private Button btnSayHi;

    private PostSayHiRequest postSayHiRequest;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SayHiFragment() {

    }


    public static SayHiFragment newInstance(String param1, String param2) {
        SayHiFragment fragment = new SayHiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_say_hi, container, false);


        //getting account id starts
        SharedPreferences prefs = getActivity().getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends

        tvUserName = view.findViewById(R.id.textview_name_say_hi);
        ivUser = view.findViewById(R.id.image_say_hi);
        etComment = view.findViewById(R.id.edittext_say_hi);
        btnSayHi = view.findViewById(R.id.button_send_say_hi);

        postSayHiRequest = new PostSayHiRequest(getContext());


        final Intent intent = getActivity().getIntent();
        member_id = intent.getStringExtra("member_id");
        member_name = intent.getStringExtra("member_name");
        member_image = intent.getStringExtra("member_image");

        tvUserName.setText(member_name);
        Picasso.with(getContext()).load(member_image).into(ivUser);


        apiCallSayHi = new ApiCallSayHi();
        data = new ArrayList<>();
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        showAlert = new ShowAlert(getContext());
        myNetworkCheck = new MyNetworkCheck();

        recyclerView = view.findViewById(R.id.recyclerview_say_hi);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        try{

            if(!myNetworkCheck.isConnected(getContext())) {
                showAlert.showWarningNetMeetMe();
            } else {
                path = BaseUrl.BASE_URL_APP +"say-hi/"+member_id;
                new GetDataFromServer().execute();
            }

        } catch (Exception e) {

        }


        btnSayHi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!myNetworkCheck.isConnected(getContext())) {
                    showAlert.showWarningNetMeetMe();
                } else{
                    path_post_comment =BaseUrl.BASE_URL_APP + "say-hi/";

                    postSayHiRequest.postData(etComment.getText().toString(), member_id, account_id,
                                                path_post_comment);
                }
            }
        });


        return view;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //Toast.makeText(context, "Say Hi fragment attached",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }



    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterSayHiComments = new RecyclerAdapterSayHiComments(getContext(), data);
            recyclerView.setAdapter(recyclerAdapterSayHiComments);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                client = new OkHttpClient();
                response = apiCallSayHi.GET(client, path, account_id);
                Log.e("##GET_SAY_HI: ", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelSayHiComments>>() {

                }.getType();

                try{

                    Collection<ModelSayHiComments> enums = gson.fromJson(response, type);
                    modelSayHiComments = enums.toArray(new ModelSayHiComments[enums.size()]);

                    if(data.isEmpty()) {
                        for(int i=0; i<enums.size(); i++) {
                            data.add(modelSayHiComments[i]);
                        }
                    }

                } catch (Exception e) {

                }




            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }



}
