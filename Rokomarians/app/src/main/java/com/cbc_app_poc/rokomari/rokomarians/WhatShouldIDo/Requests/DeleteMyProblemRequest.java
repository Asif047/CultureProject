package com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Requests;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.SuggestionsMyProblemActivity;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.WhatActivity;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DeleteMyProblemRequest {


    private String responsePost = "";
    private String response_status = "";
    private Context context;

    public DeleteMyProblemRequest(Context context) {
        this.context = context;
    }

    public void deleteData(String problemId, String account_id, String url){
        MediaType JSON = MediaType.parse("application/json; charset= utf-8");

        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, "");
        Request request = new Request.Builder()
                .url(url)
                .delete(body)
                .header("Authorization", account_id)
                .header("id", problemId)
                .addHeader("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                responsePost = ""+response.code();
                Log.e("response_post_idea: ", responsePost);

                if(responsePost.equals("200")) {

                    ((Activity)context).runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(context,"Deleted", Toast.LENGTH_SHORT).show();
                        }
                    });

                    Intent intent = new Intent(context, WhatActivity.class);
                    context.startActivity(intent);
                } else {

                    ((Activity)context).runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(context,"Delete failed", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
        });

    }


}
