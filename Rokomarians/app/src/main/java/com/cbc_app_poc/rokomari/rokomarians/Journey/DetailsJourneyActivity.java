package com.cbc_app_poc.rokomari.rokomarians.Journey;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.Journey.Adapters.RecyclerAdapterJourney;
import com.cbc_app_poc.rokomari.rokomarians.Journey.ApiCalls.ApiCallJourney;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelJourney;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

public class DetailsJourneyActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog;
    private String path, response, account_id= null;
    private OkHttpClient client;
    private ApiCallJourney apiCallJourney;
    private ModelJourney modelJourneys;
    private List<ModelJourney> data;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private TextView tvName, tvTitle, tvDetails;
    private ImageView ivUser;

    private String journey_id = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_journey);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends

        final Intent intent = getIntent();
        journey_id = intent.getStringExtra("journey_id");

        tvName = findViewById(R.id.textview_name_journey_man_details);
        tvTitle = findViewById(R.id.textview_title_journey_details);
        tvDetails = findViewById(R.id.textview_journey_details);

        ivUser = findViewById(R.id.imageview_journey_man_details);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        apiCallJourney = new ApiCallJourney();
        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);


        if(!myNetworkCheck.isConnected(this)) {
            showAlert.showWarningNetJourney();
        } else {
            try  {
                path = BaseUrl.BASE_URL_APP + "rokomarijourney/"+journey_id;
                new DetailsJourneyActivity.GetDataFromServer().execute();
            } catch (Exception e){

            }
        }


    }



    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            Picasso.with(DetailsJourneyActivity.this).load(modelJourneys.getImage()).into(ivUser);
            tvName.setText(modelJourneys.getFirstName()+" "+modelJourneys.getLastName());
            tvTitle.setText(modelJourneys.getTitle());
            tvDetails.setText(modelJourneys.getDetails());

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                client = new OkHttpClient();
                response = apiCallJourney.GET(client, path, account_id);
                Log.e("##GET_JOURNEY : ", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelJourney>>() {

                }.getType();

                modelJourneys = gson.fromJson(response, ModelJourney.class);

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;

        }
    }



}
