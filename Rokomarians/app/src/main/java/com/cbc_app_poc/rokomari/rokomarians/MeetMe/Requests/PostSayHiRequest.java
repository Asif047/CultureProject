package com.cbc_app_poc.rokomari.rokomarians.MeetMe.Requests;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.IdeaBoxActivity;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.MeetMeActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostSayHiRequest {


    private String responsePost = "";
    private String response_status = "";
    private Context context;

    private ModelSayHiResponse modelSayHiResponse;

    public PostSayHiRequest(Context context) {
        this.context = context;
    }

    public void postData(String comment, String user_id, String account_id, String url){
        MediaType JSON = MediaType.parse("application/json; charset= utf-8");
        Map<String, String> params = new HashMap<String, String>();

        params.put("comment", comment);
        params.put("userId", user_id);

        Log.e("####POST_SAY_HI: ", comment);

        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", account_id)
                .addHeader("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                responsePost = response.body().string();
                Log.e("response_say_hi: ", responsePost);

                Gson gson = new Gson();
                Type type = new TypeToken<ModelSayHiResponse>() {

                }.getType();

                modelSayHiResponse = gson.fromJson(responsePost, type);
                response_status = modelSayHiResponse.getMessage();
                Log.e("RESPONSE_POST:", response_status);




                if(response_status.equals("Comment saved.")) {

                    ((Activity)context).finish();
                    context.startActivity(((Activity)context).getIntent());

                }  else {
                    ((Activity)context).runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(context,"Comment not posted", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });

    }



}
