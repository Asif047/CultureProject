package com.cbc_app_poc.rokomari.rokomarians.ChangePassword;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.Requests.PutPasswordRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText etOldPassword, etNewPassword, etConfirmPassword;
    private Button btnSavePassword;

    private String account_id = "", user_id = "";

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private PutPasswordRequest putPasswordRequest;

    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        putPasswordRequest = new PutPasswordRequest(this);
        path = BaseUrl.BASE_URL_APP + "users/password";

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);
        String restoredUser = prefs.getString("user_id", null);


        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        if (restoredUser != null) {
            user_id = prefs.getString("user_id", "No user id defined");
        }
        //getting account id ends

        etOldPassword = findViewById(R.id.edittext_old_password);
        etNewPassword = findViewById(R.id.edittext_new_password);
        etConfirmPassword = findViewById(R.id.edittext_confirm_password);

        btnSavePassword = findViewById(R.id.button_change_password);


        btnSavePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean passwordChecked = checkPassword();

                if(passwordChecked == true) {

                    if(!etOldPassword.getText().toString().isEmpty() &&
                            !etNewPassword.getText().toString().isEmpty() &&
                            !etConfirmPassword.getText().toString().isEmpty() &&
                            (etNewPassword.getText().toString().equals(etConfirmPassword.
                                    getText().toString())) ) {

                        if(!myNetworkCheck.isConnected(ChangePasswordActivity.this)) {
                            showAlert.showWarningNetPasswordChange();
                        } else {

                            putPasswordRequest.putData(etOldPassword.getText().toString(),
                                    etConfirmPassword.getText().toString(), account_id, user_id,path);

                        }

                    }


                }



            }
        });



    }



    private boolean checkPassword() {



        if(etOldPassword.getText().toString().isEmpty()) {
            etOldPassword.setError("Please enter the old password");
            return false;
        }

        if(etNewPassword.getText().toString().isEmpty()) {
            etNewPassword.setError("Please enter the new password");
            return false;
        }

        if(etConfirmPassword.getText().toString().isEmpty()) {
            etConfirmPassword.setError("Please confirm the password");
            return false;
        }


        if(etNewPassword.getText().toString().length() < 6 || etNewPassword.getText()
                .toString().length()>32) {
            etNewPassword.setError("Character Limit should be 6 to 32");
            return false;
        }

        if(etConfirmPassword.getText().toString().length() < 6 || etConfirmPassword.getText()
                .toString().length()>32) {
            etConfirmPassword.setError("Character Limit should be 6 to 32");
            return false;
        }


        if(!etNewPassword.getText().toString().equals(etConfirmPassword.
                        getText().toString()) ){

            Toast.makeText(ChangePasswordActivity.this, "New password & Confirmed Password " +
                    "didn't match", Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;

    }


}
