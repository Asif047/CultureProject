package com.cbc_app_poc.rokomari.rokomarians.Notification;


import android.util.Log;

import com.cbc_app_poc.rokomari.rokomarians.Interfaces.NotificationApi;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ApiCallNotification implements NotificationApi {
    @Override
    public String GET(OkHttpClient client, String url, String account_id) throws IOException {
        Request request = new Request.Builder()
                                .url(url)
                                .header("Authorization", account_id)
                                .build();
        Response response = client.newCall(request).execute();

        Log.e("##TRACK_RESPONSE: ", ""+response.code());
        if(response.code() == 200) {
            return response.body().string();
        } else {
            return "invalid";
        }


    }
}
