package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.IdeaBoxActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EditGoodWorkRequest {


    private String responsePost = "", response_msg;
    private Context context;

    ModelEditGoodWorkResponse modelEditGoodWorkResponse;

    //private PostActivity postActivity=new PostActivity();

    public EditGoodWorkRequest(Context context) {
        this.context = context;
    }


    public void putData(String url, String reason, String account_id) {


        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Map<String, String> params = new HashMap<String, String>();

        params.put("reason", reason);


        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());

        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .header("Authorization", account_id)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Log.e("response", call.request().body().toString());

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

//                if (RegisterActivity.pDialog.isShowing())
//                    RegisterActivity.pDialog.dismiss();

                responsePost = response.body().string();
                Log.e("###response_code", responsePost);


                Gson gson = new Gson();
                Type type = new TypeToken<ModelEditGoodWorkResponse>() {

                }.getType();
                modelEditGoodWorkResponse = gson.fromJson(responsePost, type);
                Log.e("##RES_EDIT_NOMINEE: ", modelEditGoodWorkResponse.getMessage());

                response_msg = modelEditGoodWorkResponse.getMessage();

                if (response_msg.equals("Nomination updated.")) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show();
                        }
                    });

                    Intent intent = new Intent(context, GoodWorkActivity.class);

                    context.startActivity(intent);
                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Update failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

        });

    }


}
