package com.cbc_app_poc.rokomari.rokomarians.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelNotification {

    @SerializedName("happyPosts")
    @Expose
    private Integer happyPosts;
    @SerializedName("ideas")
    @Expose
    private Integer ideas;
    @SerializedName("nominations")
    @Expose
    private Integer nominations;

    public Integer getHappyPosts() {
        return happyPosts;
    }

    public void setHappyPosts(Integer happyPosts) {
        this.happyPosts = happyPosts;
    }

    public Integer getIdeas() {
        return ideas;
    }

    public void setIdeas(Integer ideas) {
        this.ideas = ideas;
    }

    public Integer getNominations() {
        return nominations;
    }

    public void setNominations(Integer nominations) {
        this.nominations = nominations;
    }

}
