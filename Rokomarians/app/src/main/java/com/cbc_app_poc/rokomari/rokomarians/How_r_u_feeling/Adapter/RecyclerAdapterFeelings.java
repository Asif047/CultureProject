package com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelFeeling;
import com.cbc_app_poc.rokomari.rokomarians.Model.User;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterFeelings extends RecyclerView.Adapter<RecyclerAdapterFeelings.MyViewHolder>{

    private List<ModelFeeling> modelFeelings;
    private Context context;
    private String id = "";
    private String status = "";


    public RecyclerAdapterFeelings( Context context, List<ModelFeeling> modelFeelings) {
        this.modelFeelings = modelFeelings;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_feeling,
                                            parent, false);
       return new MyViewHolder(view);
    }


    public void setfilter(List<ModelFeeling> listitem) {
        modelFeelings = new ArrayList<>();
        modelFeelings.addAll(listitem);
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvUserName.setText(modelFeelings.get(position).getUserName());

        Picasso.with(context).load(modelFeelings.get(position).getUserImage()).into(holder.ivUser);

        if(modelFeelings.get(position).getFeeling().equals("HAPPY")){
            Picasso.with(context).load(R.drawable.icon_happy).into(holder.ivFeelings);
        }
        if(modelFeelings.get(position).getFeeling().equals("VERY_HAPPY")){
            Picasso.with(context).load(R.drawable.icon_very_happy).into(holder.ivFeelings);
        }
        if(modelFeelings.get(position).getFeeling().equals("SAD")){
            Picasso.with(context).load(R.drawable.icon_sad).into(holder.ivFeelings);
        }
        if(modelFeelings.get(position).getFeeling().equals("VERY_SAD")){
            Picasso.with(context).load(R.drawable.icon_very_sad).into(holder.ivFeelings);
        }
        if(modelFeelings.get(position).getFeeling().equals("ANGRY")){
            Picasso.with(context).load(R.drawable.icon_angry).into(holder.ivFeelings);
        }
        if(modelFeelings.get(position).getFeeling().equals("NOTHING")){
            Picasso.with(context).load(R.drawable.icon_nothing).into(holder.ivFeelings);
        }

    }

    @Override
    public int getItemCount() {
        return modelFeelings.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        TextView tvUserName;
        ImageView ivUser, ivFeelings;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvUserName = itemView.findViewById(R.id.textview_user_name_feeling);
            ivUser = itemView.findViewById(R.id.image_user_feeling);
            ivFeelings = itemView.findViewById(R.id.imageview_feeling);
        }


        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }


}
