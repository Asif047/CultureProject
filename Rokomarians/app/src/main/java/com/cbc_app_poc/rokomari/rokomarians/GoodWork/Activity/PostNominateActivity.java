package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request.PostNominationRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.squareup.picasso.Picasso;

public class PostNominateActivity extends AppCompatActivity {

    private TextView tvNominatedName;
    private ImageView ivPostNomination;
    private EditText etNominatedReason;
    private Button btnAddNomination;
    private String nominated_id = "";
    private String nominated_name = "", nominated_image = "";
    private String account_id = "";

    private PostNominationRequest postNominationRequest;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_nominate);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);
        postNominationRequest = new PostNominationRequest(this);

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        //getting account id ends

        postNominationRequest = new PostNominationRequest(this);

        tvNominatedName = findViewById(R.id.textview_nominated_name);
        ivPostNomination = findViewById(R.id.imageview_post_nominated);
        etNominatedReason = findViewById(R.id.edittext_reason_nomination);
        btnAddNomination = findViewById(R.id.button_add_nomination);

        final Intent intent = getIntent();
        nominated_id = intent.getStringExtra("nominated_id");
        nominated_name = intent.getStringExtra("nominated_name");
        nominated_image = intent.getStringExtra("nominated_image");

        tvNominatedName.setText(nominated_name);
        Picasso.with(this).load(nominated_image).into(ivPostNomination);
        //Toast.makeText(PostNominateActivity.this, nominated_name+"", Toast.LENGTH_LONG).show();

        etNominatedReason.setFilters(new InputFilter[]{EMOJI_FILTER});


        btnAddNomination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!myNetworkCheck.isConnected(PostNominateActivity.this)) {
                    showAlert.showWarningNetPostNominationActivity();
                } else {
                    postNominationRequest.postData(BaseUrl.BASE_URL_APP, nominated_id, etNominatedReason.getText().toString(),
                            account_id);
                }
            }
        });

    }



    public static InputFilter EMOJI_FILTER = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE) {
                    return "";
                }
            }
            return null;
        }
    };



    //back button operation starts
    @Override
    public void onBackPressed() {
        finish();
    }
    //back button operation ends

}
