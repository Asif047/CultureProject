package com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.Adapter.RecyclerAdapterFeelings;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.ApiCalls.ApiCallFeelings;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.Adapter.RecyclerAdapterMemberList;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelFeeling;
import com.cbc_app_poc.rokomari.rokomarians.Model.User;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

public class GetFeelingsActivity extends AppCompatActivity {

    private SweetAlertDialog pDialog;
    private String path, response;
    private OkHttpClient client;
    private ModelFeeling[] modelFeelings;
    private ApiCallFeelings apiCallFeelings;
    private List<ModelFeeling> data ;
    private RecyclerView recyclerView;
    private RecyclerAdapterFeelings recyclerAdapterFeelings;
    private RecyclerView.LayoutManager layoutManager;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;
    private String account_id = null;

    private SearchView searchViewFeelings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_feelings);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends



        //search work starts
        searchViewFeelings = findViewById(R.id.search_feelings);

        searchViewFeelings.setQueryHint("Search ...");
        searchViewFeelings.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        searchViewFeelings.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!searchViewFeelings.isIconified()) {
                    searchViewFeelings.setIconified(true);
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                final List<ModelFeeling> filtermodelist = filter(data, newText);
                recyclerAdapterFeelings.setfilter(filtermodelist);
                return true;
            }
        });

        //search work ends


        apiCallFeelings = new ApiCallFeelings();
        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        recyclerView = findViewById(R.id.recyclerview_feelings);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);


        if(!myNetworkCheck.isConnected(GetFeelingsActivity.this)) {
            showAlert.showWarningNetGetFeelings();
        } else {

            try{
                path = BaseUrl.BASE_URL_APP + "feeling/feelings";
                new GetDataFromServer().execute();
            } catch (Exception e) {

            }

        }


    }



    //search work 2 starts


    private List<ModelFeeling> filter(List<ModelFeeling> pl, String query) {
        query = query.toLowerCase();
        final List<ModelFeeling> filteredModeList = new ArrayList<>();
        for (ModelFeeling model : pl) {
            final String text = model.getUserName().toLowerCase();
            if (text.startsWith(query)) {
                filteredModeList.add(model);
            }
        }
        return filteredModeList;
    }

    private void changeSearchViewTextColor(View view) {
        if (view != null) {
            if (view instanceof TextView) {
                ((TextView) view).setTextColor(Color.WHITE);
                return;
            } else if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    changeSearchViewTextColor(viewGroup.getChildAt(i));
                }
            }
        }
    }

    //search work 2 ends



    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterFeelings = new RecyclerAdapterFeelings(GetFeelingsActivity.this, data);
            recyclerView.setAdapter(recyclerAdapterFeelings);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                client = new OkHttpClient();
                response = apiCallFeelings.GET(client, path, account_id);
                Log.e("##GET_FEELINGS: ", response);
                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelFeeling>>() {

                }.getType();

                Collection<ModelFeeling> enums = gson.fromJson(response,type);
                modelFeelings = enums.toArray(new ModelFeeling[enums.size()]);

                if(data.isEmpty()) {
                    for(int i =0 ; i<enums.size(); i++) {
                        data.add(modelFeelings[i]);
                    }
                }
            } catch (IOException e){
                e.printStackTrace();
            }

            return null;
        }
    }

}
