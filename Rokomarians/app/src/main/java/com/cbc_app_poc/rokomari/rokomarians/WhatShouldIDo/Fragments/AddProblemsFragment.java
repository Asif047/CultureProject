package com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Requests.ProblemPostRequest;

import static android.content.Context.MODE_PRIVATE;

public class AddProblemsFragment extends Fragment {

    private EditText etAddProblem;
    private Button btnPostProblem;
    private ProblemPostRequest problemPostRequest;

    private String account_id =null, path="";

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_problems, container, false);


        //getting account id starts
        SharedPreferences prefs = getContext().getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends


        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(getContext());

        path = BaseUrl.BASE_URL_APP + "problems/ ";

        etAddProblem = view.findViewById(R.id.edittext_problem);
        btnPostProblem = view.findViewById(R.id.button_post_problem);
        problemPostRequest = new ProblemPostRequest(getContext());


        btnPostProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!myNetworkCheck.isConnected(getContext())) {
                    showAlert.showWarningNetWhat();
                } else {

                    if (etAddProblem.getText().toString().isEmpty()) {
                        etAddProblem.setError("Please Write some problem");
                    } else  {
                        problemPostRequest.postData(etAddProblem.getText().toString(), account_id, path);
                    }

                }



            }
        });

        return view;
    }

}
