package com.cbc_app_poc.rokomari.rokomarians.MeetMe.Adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.PostNominateActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Adapter.RecyclerAdapterNominationList;
import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.MeetMeActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.User;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterMemberList extends RecyclerView.Adapter<RecyclerAdapterMemberList.MyViewHolder> {

    private List<User> users;
    private Context context;
    private String member_id = "";
    private String member_name = "", member_image = "";

    public RecyclerAdapterMemberList(Context context, List<User> users) {
        this.users = users;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.row_item_member_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvNameNominate.setText(users.get(position).getFirstName()+ " "+
                users.get(position).getLastName());
        holder.tvTeamNominate.setText(users.get(position).getUserOfficeInfo().getTeam());

        Picasso.with(context).load(users.get(position).getImage().getImageUrl()).into(holder.ivNominate);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                member_id = ""+users.get(pos).getId();
                member_name = users.get(pos).getFirstName();
                member_image = users.get(pos).getImage().getImageUrl();

                Intent intent = new Intent(context, MeetMeActivity.class);
                intent.putExtra("member_id", member_id);
                intent.putExtra("member_name", member_name);
                intent.putExtra("member_image", member_image);

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }



    public void setfilter(List<User> listitem) {
        users = new ArrayList<>();
        users.addAll(listitem);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ItemClickListener itemClickListener;
        TextView tvNameNominate, tvTeamNominate;
        ImageView ivNominate;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvNameNominate = itemView.findViewById(R.id.textview_name_member_list);
            tvTeamNominate = itemView.findViewById(R.id.textview_team_member_list);
            ivNominate = itemView.findViewById(R.id.image_member_list);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }


}
