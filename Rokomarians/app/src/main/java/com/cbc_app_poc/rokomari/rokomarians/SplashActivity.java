package com.cbc_app_poc.rokomari.rokomarians;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.FeelingsActivity;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

public class SplashActivity extends AppCompatActivity {

    private ImageView ivSplash;
    private TextView tvNoteMe;
    private Animation fromBottom, fromTop;

    private Typeface typeface;

    PulsatorLayout pulsatorLayout;
    private String account_id = null;

    private String date_time = "", prev_date_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }


        SharedPreferences prefs2 = getSharedPreferences("Time_PREF", MODE_PRIVATE);
        prev_date_time = prefs2.getString("time", "aaa");

        if (prev_date_time != null) {
            prev_date_time = prefs2.getString("time", "No time defined");
        }

       // Toast.makeText(SplashActivity.this, prev_date_time, Toast.LENGTH_LONG).show();

        ivSplash = findViewById(R.id.imageview_splash_image);
        tvNoteMe = findViewById(R.id.textview_splash);

        fromBottom = AnimationUtils.loadAnimation(this, R.anim.frombottom);
        fromTop = AnimationUtils.loadAnimation(this, R.anim.fromtop);

        //ivSplash.setAnimation(fromTop);
       // tvNoteMe.setAnimation(fromBottom);

        //adding pulsator starts
        pulsatorLayout = findViewById(R.id.pulsator);
        pulsatorLayout.start();
        //adding pulsator ends

        //to remove the action bar starts
//        getSupportActionBar().hide();
        //to removoe the action bar ends

//        typeface = Typeface.createFromAsset(getAssets(), "font_blacklist/The Blacklist.ttf");

        //  tvNoteMe.setTypeface(typeface);



        //to get time starts
        java.util.Calendar c = java.util.Calendar.getInstance();
        java.text.SimpleDateFormat sd = new java.text.SimpleDateFormat("dd-MM-yyyy");
        date_time = sd.format(c.getTime());


        //to get time ends



        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);   // set the duration of splash screen
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

//                        Intent intent = new Intent(SplashScreenActivity.this,HomeActivity.class);
//                        startActivity(intent);
//                        finish();


                    if (account_id == null) {
                        Intent intent = new Intent(SplashActivity.this, LogInActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    else if(account_id != null && prev_date_time != null &&
                            !prev_date_time.equals(date_time)) {


                        SharedPreferences.Editor editor = getSharedPreferences("Time_PREF", MODE_PRIVATE).edit();
                        editor.putString("time", date_time);
                        editor.apply();

                        Intent intent = new Intent(SplashActivity.this, FeelingsActivity.class);
                        startActivity(intent);
                        finish();
                    }


                    else {


                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                        // overridePendingTransition(R.anim.move_right_to_left_enter, R.anim.move_right_to_left_exit);
                    }

                }
            }
        };
        timer.start();


    }


}
