package com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.DetailsIdeaActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.DetailsMyIdeaActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.EditMyIdeaActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Requests.IdeaDeleteRequest;
import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelIdea;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RecyclerAdapterMyIdeas extends RecyclerView.Adapter<RecyclerAdapterMyIdeas.MyViewHolder>{


    private List<ModelIdea> modelIdeas;
    private Context context;
    private String id = "";
    private String status = "";

    private IdeaDeleteRequest ideaDeleteRequest;
    private String account_id = "";
    private String BASE_URL_DELETE = BaseUrl.BASE_URL_APP + "idea-box/";

    public RecyclerAdapterMyIdeas(Context context, List<ModelIdea> modelIdeas) {

        this.modelIdeas = modelIdeas;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_my_ideas,
                parent, false);

        ideaDeleteRequest = new IdeaDeleteRequest(context);

        //getting account id starts
        SharedPreferences prefs = context.getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvName.setText(modelIdeas.get(position).getFirstName());
        holder.tvDate.setText(modelIdeas.get(position).getDate());
        holder.tvTitle.setText(modelIdeas.get(position).getTitle());
        holder.tvDetails.setText(modelIdeas.get(position).getIdea());

        Picasso.with(context).load(modelIdeas.get(position).getImage()).into(holder.ivIdeaMan);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Intent intent = new Intent(context, DetailsMyIdeaActivity.class);
                intent.putExtra("my_idea_id", modelIdeas.get(pos).getId());
                context.startActivity(intent);

              //  Toast.makeText(context, ""+modelIdeas.get(pos).getId(), Toast.LENGTH_SHORT).show();
            }
        });


        holder.editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditMyIdeaActivity.class);
                intent.putExtra("idea_id", modelIdeas.get(position).getId());
                intent.putExtra("my_idea", modelIdeas.get(position).getIdea());
                intent.putExtra("idea_title", modelIdeas.get(position).getTitle());
                context.startActivity(intent);
            }
        });

        holder.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)
                        .setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //finish();
                                ideaDeleteRequest.deleteData(modelIdeas.get(position).getId(), account_id,
                                        BASE_URL_DELETE);

                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
        });


    }

    @Override
    public int getItemCount() {
        return modelIdeas.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        ImageView ivIdeaMan;
        TextView tvName, tvDate, tvTitle, tvDetails;

        LinearLayout editLayout, deleteLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivIdeaMan = itemView.findViewById(R.id.imageview_rokomarian_idea);
            tvName = itemView.findViewById(R.id.textview_name_idea);
            tvDate = itemView.findViewById(R.id.textview_date_idea);
            tvTitle = itemView.findViewById(R.id.textview_title_idea);
            tvDetails = itemView.findViewById(R.id.textview_details_idea);

            editLayout = itemView.findViewById(R.id.edit_layout);
            deleteLayout = itemView.findViewById(R.id.delete_layout);

        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            this.itemClickListener.onItemClick(this.getAdapterPosition());

        }

    }



}
