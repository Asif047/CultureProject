package com.cbc_app_poc.rokomari.rokomarians.Journey.Adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Journey.DetailsJourneyActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelJourney;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerAdapterJourney extends RecyclerView.Adapter<RecyclerAdapterJourney.MyViewHolder>{

    private List<ModelJourney> modelJourneys;
    private Context context;
    private String id = "";
    private String status = "";


    public RecyclerAdapterJourney(Context context, List<ModelJourney> modelJourneys) {
        this.modelJourneys = modelJourneys;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_journey,
                                parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tvNameJourney.setText(modelJourneys.get(position).getFirstName()+" "+
                                modelJourneys.get(position).getLastName());
        holder.tvDateJourney.setText(modelJourneys.get(position).getDate());
        holder.tvTitleJourney.setText(modelJourneys.get(position).getTitle());
        holder.tvDetailsJourney.setText(modelJourneys.get(position).getDetails());

        Picasso.with(context).load(modelJourneys.get(position).getImage()).into(holder.ivUser);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                Intent intent = new Intent(context, DetailsJourneyActivity.class);
                intent.putExtra("journey_id", ""+modelJourneys.get(pos).getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelJourneys.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        ImageView ivUser;

        TextView tvNameJourney, tvDateJourney, tvTitleJourney, tvDetailsJourney;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivUser = itemView.findViewById(R.id.imageview_rokomarian_journey);
            tvNameJourney = itemView.findViewById(R.id.textview_name_journey);
            tvDateJourney = itemView.findViewById(R.id.textview_date_journey);
            tvTitleJourney = itemView.findViewById(R.id.textview_title_journey);
            tvDetailsJourney = itemView.findViewById(R.id.textview_details_journey);
        }


        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            this.itemClickListener.onItemClick(this.getLayoutPosition());

        }
    }


}
