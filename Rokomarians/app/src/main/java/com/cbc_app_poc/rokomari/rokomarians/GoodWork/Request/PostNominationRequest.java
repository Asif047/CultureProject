package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.Role;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostNominationRequest {

    private String responsePost = "";
    private String response_status;
    private Context context;
    private Role.User user;
    //private PostActivity postActivity=new PostActivity();

    private ModelResponseNomination modelResponseNomination;

    public PostNominationRequest(Context context) {
        this.context = context;
    }

    public void postData(String url,String nominated_id, String reason, String account_id) {

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");


        //new starts

        Map<String, String> params = new HashMap<String, String>();

        params.put("nominatedUserId", nominated_id);
        params.put("reason", reason);

        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());

        //new ends

        Request request = new Request.Builder()
                .url(url+"nomination/new-nomination")
                .post(body)
                .header("Authorization", account_id)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Log.e("response", call.request().body().toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

//                if (RegisterActivity.pDialog.isShowing())
//                    RegisterActivity.pDialog.dismiss();

                modelResponseNomination = new ModelResponseNomination();

                responsePost = response.body().string();
                Log.e("###response_code", responsePost);

                Gson gson = new Gson();
                Type type = new TypeToken<ModelResponseNomination>() {

                }.getType();

                modelResponseNomination = gson.fromJson(responsePost, type);

                response_status = modelResponseNomination.getMessage();

                Log.e("##RESPONSE_NOMINATION: ", response_status);

                if(response_status.equals("New nomination created.")) {

                    Intent intent = new Intent(context, GoodWorkActivity.class);

                    context.startActivity(intent);
                } else if(response_status.equals("User can't nominate himself.")) {

                    ((Activity)context).runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(context,"User can't nominate himself", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                else
                {
                    ((Activity)context).runOnUiThread(new Runnable()
                    {
                        public void run()
                        {
                            Toast.makeText(context,"Nomination not submitted", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

        });

    }

}
