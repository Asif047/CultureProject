package com.cbc_app_poc.rokomari.rokomarians.ForgetPassword;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.Requests.PostSendEmailRequest;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

public class ForgetPasswordActivity extends AppCompatActivity {


    private EditText etEnterEmail;
    private Button btnNextEmail;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;
    private PostSendEmailRequest postSendEmailRequest;

    private String path;

    public static SweetAlertDialog pDialogForgetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);


        pDialogForgetPassword = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        etEnterEmail = findViewById(R.id.edittext_reset_email);
        btnNextEmail = findViewById(R.id.button_next_enter_email);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        postSendEmailRequest = new PostSendEmailRequest(this);
        path = BaseUrl.BASE_URL_APP + "forget_password/get_code";

        btnNextEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etEnterEmail.getText().toString().isEmpty()) {
                    etEnterEmail.setError("Please enter your email");
                }

                if(!etEnterEmail.getText().toString().isEmpty()) {
                    if(!myNetworkCheck.isConnected(ForgetPasswordActivity.this)) {
                        showAlert.showWarningNetMeetMe();
                    } else {
                        pDialogForgetPassword.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                        pDialogForgetPassword.setTitleText("Loading");
                        pDialogForgetPassword.setCancelable(false);
                        pDialogForgetPassword.show();

                         postSendEmailRequest.postData(etEnterEmail.getText().toString(),
                                path);

                    }
                }
            }
        });

    }



    //back button operation starts

    @Override
    public void onBackPressed() {
       finish();
       Intent intent = new Intent(ForgetPasswordActivity.this, LogInActivity.class);
       startActivity(intent);
    }

    //back button operation ends


}
