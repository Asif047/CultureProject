package com.cbc_app_poc.rokomari.rokomarians.ChangePassword.Requests;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.IdeaBoxActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelMember;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PutPasswordRequest {

    private String responsePost = "";
    private String response_status = "";
    private Context context;

    private ModelChangePassword modelChangePassword;

    public PutPasswordRequest(Context context) {
        this.context = context;
    }

    public void putData(String oldPassword, String newPassword, String account_id, String id,
                         String url){
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Map<String, String> params = new HashMap<String, String>();

        params.put("newPassword", newPassword);
        params.put("oldPassword", oldPassword);

        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());

//        OkHttpClient client = new OkHttpClient();
//
//        RequestBody body = RequestBody.create(JSON,  "{\r\n\t\"id\":"+idea_id+"," +
//                "\r\n\t\"title\":\""+title+"\"," +
//                "\r\n\t\"idea\":\""+idea+"\"\r\n}");


        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .header("Authorization", account_id)
                .header("id", id)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Log.e("response", call.request().body().toString());

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

//                if (RegisterActivity.pDialog.isShowing())
//                    RegisterActivity.pDialog.dismiss();

                responsePost = response.body().string();

                Gson gson = new Gson();
                Type type = new TypeToken<ModelChangePassword>() {

                }.getType();

                modelChangePassword = gson.fromJson(responsePost, type);

                Log.e("###response_code", modelChangePassword.getMessage());


                if (modelChangePassword.getMessage().equals("Password updated")) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show();
                        }
                    });

                    Intent intent = new Intent(context, HomeActivity.class);

                    context.startActivity(intent);
                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Update failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }

        });

    }


}
