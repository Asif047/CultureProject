package com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;

public class FeelingsActivity extends AppCompatActivity {

    private AlphaAnimation buttonClick;
    private ImageView ivHappy, ivVeryHappy, ivSad, ivVerySad, ivAngry, ivNothing;
    private Button btnPostFeeling;

    private PostFeelingRequest postFeelingRequest;
    private String account_id = "", path="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feelings);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends

        path = BaseUrl.BASE_URL_APP + "feeling/";

        postFeelingRequest = new PostFeelingRequest(this);

        btnPostFeeling = findViewById(R.id.button_post_feelings);
        ivHappy = findViewById(R.id.imageview_happy);
        ivVeryHappy = findViewById(R.id.imageview_very_happy);
        ivSad = findViewById(R.id.imageview_sad);
        ivVerySad = findViewById(R.id.imageview_very_sad);
        ivAngry = findViewById(R.id.imageview_angry);
        ivNothing = findViewById(R.id.imageview_nothing);
        buttonClick = new AlphaAnimation(1F, 0.8F);


        ivHappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFeelingRequest.postData("HAPPY", account_id, path);
            }
        });

        ivVeryHappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFeelingRequest.postData("VERY_HAPPY", account_id, path);
            }
        });

        ivSad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFeelingRequest.postData("SAD", account_id, path);
            }
        });

        ivVerySad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFeelingRequest.postData("VERY_SAD", account_id, path);
            }
        });

        ivAngry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFeelingRequest.postData("ANGRY", account_id, path);
            }
        });

        ivNothing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postFeelingRequest.postData("NOTHING", account_id, path);
            }
        });


    }
}
