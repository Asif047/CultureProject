package com.cbc_app_poc.rokomari.rokomarians.Interfaces;


import java.io.IOException;

import okhttp3.OkHttpClient;

public interface JourneyApi {

    public String GET(OkHttpClient client, String url, String token) throws IOException;

}
