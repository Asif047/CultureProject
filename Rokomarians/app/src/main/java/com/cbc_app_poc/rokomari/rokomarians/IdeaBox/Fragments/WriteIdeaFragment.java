package com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Requests.IdeaPostRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;

import static android.content.Context.MODE_PRIVATE;


public class WriteIdeaFragment extends Fragment {


    private EditText etTitle, etIdea;
    private IdeaPostRequest ideaPostRequest;
    private Button btnAddIdea;

    private String account_id = "";
    private static final String BASE_URL = BaseUrl.BASE_URL_APP+"idea-box/";

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_write_idea, container, false);


        //getting account id starts
        SharedPreferences prefs = getActivity().getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        //getting account id ends


        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(getContext());

        etTitle = view.findViewById(R.id.edittext_title_idea);
        etIdea = view.findViewById(R.id.edittext_idea);
        btnAddIdea = view.findViewById(R.id.button_add_idea);

        etIdea.setFilters(new InputFilter[]{EMOJI_FILTER});
        etTitle.setFilters(new InputFilter[]{EMOJI_FILTER});

        ideaPostRequest = new IdeaPostRequest(getContext());
        btnAddIdea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!myNetworkCheck.isConnected(getContext())) {
                    showAlert.showWarningNetIdeaBoxActivity();
                } else{

                    if(etIdea.getText().toString().isEmpty()) {
                        etIdea.setError("Please write something");
                    } else {
                        ideaPostRequest.postData(etTitle.getText().toString(), etIdea.getText().toString(),
                                account_id, BASE_URL);
                    }

                }

            }
        });

        return view;
    }



    public static InputFilter EMOJI_FILTER = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE) {
                    return "";
                }
            }
            return null;
        }
    };


}
