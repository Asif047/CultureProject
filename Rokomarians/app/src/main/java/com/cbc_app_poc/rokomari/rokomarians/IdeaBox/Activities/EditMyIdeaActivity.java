package com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Requests.UpdateIdeaRequest;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;

public class EditMyIdeaActivity extends AppCompatActivity {

    private int idea_id;
    private EditText etTitle, etIdea;
    private Button btnUpdate;

    private UpdateIdeaRequest updateIdeaRequest;
    private static final String URL_UPDATE_IDEA = BaseUrl.BASE_URL_APP+"idea-box/";

    private String account_id = "", my_idea = "", idea_title="";
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_my_idea);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        //getting account id ends

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        etTitle = findViewById(R.id.edittext_title_idea_edit);
        etIdea = findViewById(R.id.edittext_idea_edit);
        btnUpdate = findViewById(R.id.button_add_idea);

        etIdea.setFilters(new InputFilter[]{EMOJI_FILTER});
        etTitle.setFilters(new InputFilter[]{EMOJI_FILTER});

        updateIdeaRequest = new UpdateIdeaRequest(this);

        final Intent intent = getIntent();
        idea_id = intent.getIntExtra("idea_id", 0);
        my_idea = intent.getStringExtra("my_idea");
        idea_title = intent.getStringExtra("idea_title");

        etTitle.setText(idea_title);
        etIdea.setText(my_idea);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!myNetworkCheck.isConnected(EditMyIdeaActivity.this)){
                    showAlert.showWarningNetEditMyIdea();
                } else {
                    updateIdeaRequest.putData(URL_UPDATE_IDEA, idea_id, etTitle.getText().toString(),
                            etIdea.getText().toString(), account_id);
                }

            }
        });
    }




    public static InputFilter EMOJI_FILTER = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE) {
                    return "";
                }
            }
            return null;
        }
    };


    //back button operation starts
    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(EditMyIdeaActivity.this, IdeaBoxActivity.class);
//        startActivity(intent);
        finish();
    }
    //back button operation ends


}
