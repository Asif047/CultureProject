package com.cbc_app_poc.rokomari.rokomarians.HappyWall.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Requests.UpdateHappyPutRequest;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.squareup.picasso.Picasso;

public class UpdateHappyPostActivity extends AppCompatActivity {

    private TextView tvName;
    private ImageView ivUser;
    private EditText etDetails;
    private Button btnUpdate;

    private String account_id = "", happy_post = "", user_image="";
    private UpdateHappyPutRequest updateHappyPutRequest;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private String post_id= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_happy_post);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        final Intent intent = getIntent();
        post_id = ""+intent.getIntExtra("post_id", 0);
        happy_post = intent.getStringExtra("happy_post");

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }


        String restoredImage = prefs.getString("user_image", null);

        if (restoredImage != null) {
            user_image = prefs.getString("user_image", "No image defined");
        }

        //getting account id ends

        updateHappyPutRequest = new UpdateHappyPutRequest(this);

        tvName = findViewById(R.id.textview_name_update_happy_post);
        ivUser = findViewById(R.id.imageview_update_happy_post);
        etDetails = findViewById(R.id.edittext_update_happy_post);
        btnUpdate = findViewById(R.id.button_update_happy_post);

        etDetails.setText(happy_post);
        Picasso.with(this).load(user_image).into(ivUser);


        etDetails.setFilters(new InputFilter[]{EMOJI_FILTER});

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!myNetworkCheck.isConnected(UpdateHappyPostActivity.this)){
                      showAlert.showWarningNetUpdateHappyPost();
                } else {
                    updateHappyPutRequest.putData(BaseUrl.BASE_URL_APP, account_id, post_id, etDetails.getText().toString());
                }

            }
        });

    }




    public static InputFilter EMOJI_FILTER = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int index = start; index < end; index++) {

                int type = Character.getType(source.charAt(index));

                if (type == Character.SURROGATE) {
                    return "";
                }
            }
            return null;
        }
    };


    //back button operation starts
    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(UpdateHappyPostActivity.this, HappyWallActivity.class);
//        startActivity(intent);
        finish();
    }
    //back button operation ends


}
