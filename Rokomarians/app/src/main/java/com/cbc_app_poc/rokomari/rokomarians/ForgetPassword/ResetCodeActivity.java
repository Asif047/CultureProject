package com.cbc_app_poc.rokomari.rokomarians.ForgetPassword;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.Requests.PostResetCodeRequest;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;

public class ResetCodeActivity extends AppCompatActivity {

    private EditText etResetCode;
    private Button btnResetCode;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private PostResetCodeRequest postResetCodeRequest;
    private String path, cookie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_code);

        SharedPreferences prefs = getSharedPreferences("Cookie_PREF", MODE_PRIVATE);
        String restoredCookie = prefs.getString("cookie", null);

        if(restoredCookie != null) {
            cookie = prefs.getString("cookie", "no cookie defined");
        }

       // Toast.makeText(ResetCodeActivity.this, cookie, Toast.LENGTH_LONG).show();

        etResetCode = findViewById(R.id.edittext_reset_code);
        btnResetCode = findViewById(R.id.button_next_reset_code);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        postResetCodeRequest = new PostResetCodeRequest(this);
        path = BaseUrl.BASE_URL_APP +"forget_password/submit_code";

        btnResetCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etResetCode.getText().toString().isEmpty()) {
                    etResetCode.setError("Please enter your code");
                }

                if(!etResetCode.getText().toString().isEmpty()) {
                    if(!myNetworkCheck.isConnected(ResetCodeActivity.this)) {
                        showAlert.showWarningNetMeetMe();
                    } else {
                        postResetCodeRequest.postData(etResetCode.getText().toString(), path, cookie);

                    }
                }

            }
        });

    }



    //back button operation starts

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(ResetCodeActivity.this, ForgetPasswordActivity.class);
        startActivity(intent);
    }

    //back button operation ends



}
