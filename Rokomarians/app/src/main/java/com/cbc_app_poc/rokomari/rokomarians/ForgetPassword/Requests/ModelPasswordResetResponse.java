package com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.Requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelPasswordResetResponse {

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
