package com.cbc_app_poc.rokomari.rokomarians.MeetMe.Adapter;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.EditCommentActivity;
import com.cbc_app_poc.rokomari.rokomarians.MeetMe.Requests.DeleteSayHiComments;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelSayHiComments;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RecyclerAdapterSayHiComments extends RecyclerView.Adapter
        <RecyclerAdapterSayHiComments.MyViewHolder> {

    private List<ModelSayHiComments> modelSayHiComments;
    private Context context;
    private String id = "", account_id="", user_id="", path= "";
    private String status = "";

    private DeleteSayHiComments deleteSayHiComments;

    public RecyclerAdapterSayHiComments( Context context, List<ModelSayHiComments> modelSayHiComments) {
        this.modelSayHiComments = modelSayHiComments;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_say_hi,
                                            parent, false);


        //getting user id starts
        SharedPreferences prefs = context.getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredUserId = prefs.getString("user_id", null);

        if (restoredUserId != null) {
            user_id = prefs.getString("user_id", "No user id defined");
        }


        String restoredAccountId = prefs.getString("account_id", null);

        if (restoredAccountId != null) {
            account_id = prefs.getString("account_id", "No user id defined");
        }

        //getting user id ends


        path = BaseUrl.BASE_URL_APP +"say-hi/";

        deleteSayHiComments = new DeleteSayHiComments(context);

        return  new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvNameCommentor.setText(modelSayHiComments.get(position).getCommenterName());
        holder.tvComment.setText(modelSayHiComments.get(position).getComment());

        Picasso.with(context).load(modelSayHiComments.get(position).getCommenterImage())
                .into(holder.ivCommentor);


        holder.ivCommentDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_id.equals(modelSayHiComments.get(position).getCommenterId())) {

                    deleteSayHiComments.deleteData(modelSayHiComments.get(position).getCommentId(),
                                                    account_id, path);

                } else {
                    Toast.makeText(context, "You can't Delete other's post", Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.ivCommentEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_id.equals(modelSayHiComments.get(position).getCommenterId())) {

                    Intent intent = new Intent(context, EditCommentActivity.class);
                    intent.putExtra("comment_id", modelSayHiComments.get(position).getCommentId());
                    intent.putExtra("comment", modelSayHiComments.get(position).getComment());
                    context.startActivity(intent);

                } else {
                    Toast.makeText(context, "You can't Edit other's post", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return modelSayHiComments.size();
    }



    public class MyViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        TextView tvNameCommentor, tvComment;
        ImageView ivCommentor, ivCommentDelete, ivCommentEdit;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvNameCommentor = itemView.findViewById(R.id.textview_name_say_hi_commentor);
            tvComment = itemView.findViewById(R.id.textview_comment_say_hi);
            ivCommentor = itemView.findViewById(R.id.imageview_say_hi_commentor);

            ivCommentDelete = itemView.findViewById(R.id.imageview_say_hi_comment_delete);
            ivCommentEdit = itemView.findViewById(R.id.imageview_say_hi_comment_edit);

        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());

        }
    }


}
