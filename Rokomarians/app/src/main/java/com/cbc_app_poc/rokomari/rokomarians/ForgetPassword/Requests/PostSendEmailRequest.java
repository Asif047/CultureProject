package com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.Requests;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.Requests.ModelChangePassword;
import com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.ForgetPasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.ResetCodeActivity;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostSendEmailRequest {

    private String responsePost = "";
    private String response_status = "", cookie="";
    private Context context;


    private ModelSendEmailResponse modelSendEmailResponse;

    public PostSendEmailRequest(Context context) {
        this.context = context;
    }

    public void postData(String email, String url){
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Map<String, String> params = new HashMap<String, String>();

        params.put("email", email);


        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());

//        OkHttpClient client = new OkHttpClient();
//
//        RequestBody body = RequestBody.create(JSON,  "{\r\n\t\"id\":"+idea_id+"," +
//                "\r\n\t\"title\":\""+title+"\"," +
//                "\r\n\t\"idea\":\""+idea+"\"\r\n}");


        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Log.e("response", call.request().body().toString());
                ForgetPasswordActivity.pDialogForgetPassword.dismiss();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

//                if (RegisterActivity.pDialog.isShowing())
//                    RegisterActivity.pDialog.dismiss();

                responsePost = response.body().string();
                cookie = response.header("Set-Cookie");


                SharedPreferences.Editor editor = context.getSharedPreferences("Cookie_PREF",
                        Context.MODE_PRIVATE).edit();
                editor.putString("cookie", cookie);
                editor.apply();

                Gson gson = new Gson();
                Type type = new TypeToken<ModelSendEmailResponse>() {

                }.getType();

                modelSendEmailResponse = gson.fromJson(responsePost, type);

                Log.e("###response_code", modelSendEmailResponse.getMessage());


                if (modelSendEmailResponse.getMessage().equals("An email with verification code sent to your email.")) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Email sent", Toast.LENGTH_SHORT).show();
                            ForgetPasswordActivity.pDialogForgetPassword.dismiss();

                        }
                    });

                    Intent intent = new Intent(context, ResetCodeActivity.class);

                    context.startActivity(intent);


                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Email sent failed", Toast.LENGTH_SHORT).show();
                            ForgetPasswordActivity.pDialogForgetPassword.dismiss();

                        }
                    });
                }

            }

        });



    }



}
