package com.cbc_app_poc.rokomari.rokomarians.HappyWall.Adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Activity.DetailsMyHappyPostActivity;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Activity.UpdateHappyPostActivity;
import com.cbc_app_poc.rokomari.rokomarians.HappyWall.Requests.DeleteHappyPostRequest;
import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelHappySeeAll;
import com.cbc_app_poc.rokomari.rokomarians.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RecylerAdapterMyHappyPost extends RecyclerView.Adapter<RecylerAdapterMyHappyPost.MyViewHolder>{

    private List<ModelHappySeeAll> modelHappySeeAlls;
    private Context context;
    private int id = 0;
    private String status = "";

    private int post_id = 0;
    private ModelHappySeeAll modelHappySeeAll;
    private DeleteHappyPostRequest deleteHappyPostRequest;
    private String account_id = "";

    public RecylerAdapterMyHappyPost( Context context, List<ModelHappySeeAll> modelHappySeeAlls) {
        this.modelHappySeeAlls = modelHappySeeAlls;
        this.context = context;
    }


    @Override
    public RecylerAdapterMyHappyPost.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_my_happy_post, parent, false);

        modelHappySeeAll = new ModelHappySeeAll();
        deleteHappyPostRequest = new DeleteHappyPostRequest(context);

        //getting account id starts
        SharedPreferences prefs = context.getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }
        //getting account id ends


        return  new RecylerAdapterMyHappyPost.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecylerAdapterMyHappyPost.MyViewHolder holder, final int position) {

        holder.tvName.setText(modelHappySeeAlls.get(position).getName());
        holder.tvDetails.setText(modelHappySeeAlls.get(position).getDetails());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                id = modelHappySeeAlls.get(pos).getId();

                Intent intent = new Intent(context, DetailsMyHappyPostActivity.class);
                intent.putExtra("happy_post_id", id);
                Log.e("###POST_ID: ", ""+id);
                context.startActivity(intent);
            }
        });

        holder.editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UpdateHappyPostActivity.class);
                intent.putExtra("post_id", modelHappySeeAlls.get(position).getId());
                intent.putExtra("happy_post", modelHappySeeAlls.get(position).getDetails());
                context.startActivity(intent);
            }
        });


        holder.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(context)
                        .setMessage("Are you sure you want to delete?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                //finish();
                                deleteHappyPostRequest.deleteData(BaseUrl.BASE_URL_APP, account_id,
                                        modelHappySeeAlls.get(position).getId());

                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
        });


    }

    @Override
    public int getItemCount() {
        return modelHappySeeAlls.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvName, tvDetails;
        ItemClickListener itemClickListener;

        LinearLayout editLayout, deleteLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textview_name_see_all);
            tvDetails = itemView.findViewById(R.id.textview_happy_post_see_all);

            editLayout = itemView.findViewById(R.id.edit_layout);
            deleteLayout = itemView.findViewById(R.id.delete_layout);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }

}
