package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.ChangePasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.ApiCalls.ApiCallNominatedWork;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Adapter.RecyclerAdapterNominated;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.GetFeelingsActivity;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelNominated;
import com.cbc_app_poc.rokomari.rokomarians.Profile.ProfileActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

public class GoodWorkActivity extends AppCompatActivity {

    private FloatingActionButton btnAddGoodWork;
    private SweetAlertDialog pDialog;
    private String path;
    private OkHttpClient client;
    private String response;
    private ModelNominated[] modelNominateds;
    private ApiCallNominatedWork apiCallNominatedWork;
    private List<ModelNominated> data;
    private RecyclerView recyclerView;
    private RecyclerAdapterNominated recyclerAdapterNominated;
    private RecyclerView.LayoutManager layoutManager;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    NavigationView nvDrawer;

    private String account_id = null, user_email = "", user_name = "", user_image = "";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_good_work);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        String restoredName = prefs.getString("user_name", null);

        if (restoredName != null) {
            user_name = prefs.getString("user_name", "No account defined");
        }

        String restoredEmail = prefs.getString("user_email", null);

        if (restoredEmail != null) {
            user_email = prefs.getString("user_email", "No account defined");
        }

        String restoredImage = prefs.getString("user_image", null);

        if (restoredImage != null) {
            user_image = prefs.getString("user_image", "No account defined");
        }

        //getting account id ends


        getSupportActionBar().hide();
        toolbar = (Toolbar) findViewById(R.id.toolbar_good_work);
        toolbar.setTitle("Good Work");


        // navigation drawer work starts

        dl = (DrawerLayout) findViewById(R.id.dl);
        nvDrawer = (NavigationView) findViewById(R.id.nv);

        toggle = new ActionBarDrawerToggle(this, dl, toolbar, R.string.open, R.string.close);
        dl.addDrawerListener(toggle);

        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        setupDrawerContent(nvDrawer);

        // navigation drawer work ends


        //new starts
        View hView = nvDrawer.getHeaderView(0);
        TextView nav_user_name = (TextView) hView.findViewById(R.id.textview_name_header);
        TextView nav_user_email = (TextView) hView.findViewById(R.id.textview_email_header);
        ImageView nav_user_image = (CircleImageView) hView.findViewById(R.id.image_header);

        nav_user_name.setText(user_name);
        nav_user_email.setText(user_email);
        Picasso.with(this).load(user_image).into(nav_user_image);

        //new ends


        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        recyclerView = findViewById(R.id.recyclerview_good_work);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        apiCallNominatedWork = new ApiCallNominatedWork();
        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        btnAddGoodWork = findViewById(R.id.button_add_good_work);
        btnAddGoodWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GoodWorkActivity.this, NominateActivity.class);
                startActivity(intent);
            }
        });


        try{
            path = BaseUrl.BASE_URL_APP + "nomination/";

            if(!myNetworkCheck.isConnected(this)){
                showAlert.showWarningNetGoodWorkActivity();
            } else {
                new GetDataFromServer().execute();
            }

        } catch (Exception e){

        }

    }


    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
               pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterNominated = new RecyclerAdapterNominated(GoodWorkActivity.this, data, account_id);
            recyclerView.setAdapter(recyclerAdapterNominated);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                client = new OkHttpClient();
                response = apiCallNominatedWork.GET(client, path, account_id);
                Log.e("##SHOW_NOMINATEDS: ", response);
                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelNominated>>() {

                }.getType();

                Collection<ModelNominated> enums = gson.fromJson(response, type);
                modelNominateds = enums.toArray(new ModelNominated[enums.size()]);

                if(data.isEmpty()){
                    for(int i = 0; i<enums.size(); i++){
                        data.add(modelNominateds[i]);
                    }
                }

            } catch (IOException e){
                e.printStackTrace();
            }

            return null;

        }
    }






    public void selectItemDrawer(MenuItem menuItem) {
        Fragment myFragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(GoodWorkActivity.this, HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.profile:
                Intent intent1 = new Intent(GoodWorkActivity.this, ProfileActivity.class);
                startActivity(intent1);
                break;

            case R.id.log_out_drawer:

                account_id = null;
                SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                editor.putString("account_id", account_id);
                editor.apply();

                Intent intent2 = new Intent(GoodWorkActivity.this, LogInActivity.class);
                startActivity(intent2);

                break;



            case R.id.change_password:

                Intent intent3 = new Intent(GoodWorkActivity.this, ChangePasswordActivity.class);
                startActivity(intent3);

                break;

            case R.id.mood_meter :
                Intent intent4 = new Intent(GoodWorkActivity.this, GetFeelingsActivity.class);
                startActivity(intent4);
                break;


            default:
                // fragmentClass = Network.class;
        }
        try {
//            myFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.flcontent,myFragment).commit();
        menuItem.setChecked(true);
        //setTitle(menuItem.getTitle());
        dl.closeDrawers();


    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItemDrawer(item);
                return true;
            }
        });
    }


    //back button operation starts
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(GoodWorkActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
    //back button operation ends


}
