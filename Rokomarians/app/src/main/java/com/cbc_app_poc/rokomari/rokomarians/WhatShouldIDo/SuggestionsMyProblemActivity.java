package com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelSuggestion;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Adapters.RecyclerAdapterSuggestions;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.ApiCalls.ApiCallSuggestion;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Requests.DeleteMyProblemRequest;
import com.cbc_app_poc.rokomari.rokomarians.WhatShouldIDo.Requests.SuggestionPostRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.OkHttpClient;

public class SuggestionsMyProblemActivity extends AppCompatActivity {

    private TextView tvProblemDetails, tvProblemNum;
    private String problem = "", problem_num = "", problem_id = "";

    private SweetAlertDialog pDialog;
    private String path="", path_delete_problem="", response;
    private OkHttpClient client;
    private ModelSuggestion[] modelSuggestions;
    private ApiCallSuggestion apiCallSuggestion;
    private List<ModelSuggestion> data;

    private RecyclerView recyclerView;
    private RecyclerAdapterSuggestions recyclerAdapterSuggestions;
    private RecyclerView.LayoutManager layoutManager;

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private String account_id = null, user_id = "";

    private DeleteMyProblemRequest deleteMyProblemRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions_my_problem);


        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }


        String restoredId = prefs.getString("user_id", null);

        if (restoredId != null) {
            user_id = prefs.getString("user_id", "No user id defined");
        }


        //getting account id ends

        apiCallSuggestion = new ApiCallSuggestion();
        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        deleteMyProblemRequest = new DeleteMyProblemRequest(this);

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        recyclerView = findViewById(R.id.recyclerview_my_suggestions);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        tvProblemDetails = findViewById(R.id.textview_my_problem_details);
        tvProblemNum = findViewById(R.id.textview_my_problem_num_details);

        final Intent intent = getIntent();
        problem_num = intent.getStringExtra("problem_num");
        problem = intent.getStringExtra("problem");
        problem_id = intent.getStringExtra("problem_id");

        tvProblemNum.setText(problem_num);
        tvProblemDetails.setText(problem);

        path_delete_problem = BaseUrl.BASE_URL_APP+"problems/";

       // Toast.makeText(SuggestionsMyProblemActivity.this, problem_id, Toast.LENGTH_LONG).show();

        try {
            if(!myNetworkCheck.isConnected(SuggestionsMyProblemActivity.this)) {
                showAlert.showWarningNetWhat();
            } else {
                path = BaseUrl.BASE_URL_APP+"suggestion/problem/"+problem_id;
                new SuggestionsMyProblemActivity.GetDataFromServer().execute();
            }
        } catch (Exception e){

        }

    }

    public void deleteProblem(MenuItem item) {

        if(!myNetworkCheck.isConnected(SuggestionsMyProblemActivity.this)) {
            showAlert.showWarningNetSuggestion();
        } else {
            deleteMyProblemRequest.deleteData(problem_id, account_id, path_delete_problem);
        }

    }


    public class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }

            recyclerAdapterSuggestions = new RecyclerAdapterSuggestions(SuggestionsMyProblemActivity.this, data,
                                                account_id, user_id);
            recyclerView.setAdapter(recyclerAdapterSuggestions);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                client = new OkHttpClient();
                response = apiCallSuggestion.GET(client, path, account_id);

                Log.e("###GET_SUGGESTION: ", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelSuggestion>>() {

                }.getType();

                Collection<ModelSuggestion> enums = gson.fromJson(response, type);
                modelSuggestions = enums.toArray(new ModelSuggestion[enums.size()]);

                if(data.isEmpty()) {
                    for(int i= 0; i< enums.size(); i++) {
                        data.add(modelSuggestions[i]);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete_problem, menu);

        return true;
    }
}
