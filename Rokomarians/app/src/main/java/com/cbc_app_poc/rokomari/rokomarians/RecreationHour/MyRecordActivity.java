package com.cbc_app_poc.rokomari.rokomarians.RecreationHour;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ChangePassword.ChangePasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.How_r_u_feeling.GetFeelingsActivity;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelRecreationMyrecord;
import com.cbc_app_poc.rokomari.rokomarians.Profile.ProfileActivity;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

public class MyRecordActivity extends AppCompatActivity {

    private TextView tvCategory, tvdetails, tvName;
    private ImageView ivProfile;

    private SweetAlertDialog pDialog;
    private String path, path_recreation, response, response_recreation;
    private ModelRecreationMyrecord modelRecreationMyrecords;
    private ApiCallRecreationMyRecord apiCallRecreationMyRecord;
    private PartcipateDeleteRequest partcipateDeleteRequest;
    private List<ModelRecreationMyrecord> data;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;
    private OkHttpClient client;

    private CardView cardMyRecord;
    private FloatingActionButton btnAddParticipate;

    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    NavigationView nvDrawer;

    private String account_id = null, user_email = "", user_name = "", user_image = "";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_record);

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);


        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        String restoredName = prefs.getString("user_name", null);

        if (restoredName != null) {
            user_name = prefs.getString("user_name", "No account defined");
        }

        String restoredEmail = prefs.getString("user_email", null);

        if (restoredEmail != null) {
            user_email = prefs.getString("user_email", "No account defined");
        }

        String restoredImage = prefs.getString("user_image", null);

        if (restoredImage != null) {
            user_image = prefs.getString("user_image", "No account defined");
        }
        //getting account id ends

        apiCallRecreationMyRecord = new ApiCallRecreationMyRecord();

        data = new ArrayList<>();
        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        tvCategory = findViewById(R.id.textview_category_recreation_my_record);
        tvdetails = findViewById(R.id.textview_details_recreation_my_record);
        tvName = findViewById(R.id.textview_name_recreation_my_record);
        ivProfile = findViewById(R.id.imageview_recreation_list);
        cardMyRecord = findViewById(R.id.card_my_record);
        btnAddParticipate = findViewById(R.id.button_add_participate);

        partcipateDeleteRequest = new PartcipateDeleteRequest(this);

        tvName.setText(user_name);
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        if(!myNetworkCheck.isConnected(MyRecordActivity.this)){
            showAlert.showWarningNetMyRecordActivity();
        } else {
             try{
                path = BaseUrl.BASE_URL_APP + "participation/my-record";
                path_recreation = BaseUrl.BASE_URL_APP + "recreation-hours/";
                new GetDataFromServer().execute();
                new GetRecreationHourFromServer().execute();
            } catch (Exception e){

            }
        }


        cardMyRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!tvCategory.getText().toString().equals("")){
                    Intent intent = new Intent(MyRecordActivity.this, EditRecreationHourActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MyRecordActivity.this, "Click Add to Participate",Toast.LENGTH_SHORT).show();
                }

            }
        });


        toolbar = (Toolbar) findViewById(R.id.toolbar_good_work);
        toolbar.setTitle("My Participation");


        // navigation drawer work starts

        dl = (DrawerLayout) findViewById(R.id.dl);
        nvDrawer = (NavigationView) findViewById(R.id.nv);

        toggle = new ActionBarDrawerToggle(this, dl, toolbar, R.string.open, R.string.close);
        dl.addDrawerListener(toggle);

        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        setupDrawerContent(nvDrawer);

        // navigation drawer work ends


        //new starts
        View hView = nvDrawer.getHeaderView(0);
        TextView nav_user_name = (TextView) hView.findViewById(R.id.textview_name_header);
        TextView nav_user_email = (TextView) hView.findViewById(R.id.textview_email_header);
        ImageView nav_user_image = (CircleImageView) hView.findViewById(R.id.image_header);

        nav_user_name.setText(user_name);
        nav_user_email.setText(user_email);
        Picasso.with(this).load(user_image).into(nav_user_image);

        //new ends



    }

    public void updateParticipate(MenuItem item) {

        if(!tvCategory.getText().toString().equals("")){
            Intent intent = new Intent(MyRecordActivity.this, EditRecreationHourActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(MyRecordActivity.this, "Click Add to Participate",Toast.LENGTH_SHORT).show();
        }

    }

    public void DeleteParticipate(MenuItem item) {
        partcipateDeleteRequest.deleteData(BaseUrl.BASE_URL_APP, account_id);

    }


    private class GetDataFromServer extends AsyncTask< Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()){
                pDialog.dismiss();
            }

            try{
                tvCategory.setText(modelRecreationMyrecords.getSelectedCategories());
                tvdetails.setText(modelRecreationMyrecords.getDetails());
                Picasso.with(MyRecordActivity.this).load(user_image).into(ivProfile);

                if(tvCategory.getText().toString().equals("")){
                    btnAddParticipate.setVisibility(View.VISIBLE);
                }

            } catch (Exception e){

            }


        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
             client = new OkHttpClient();
             response = apiCallRecreationMyRecord.GET(client, path, account_id);
             Log.e("###GET_MY_RECORD:", response);

             Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelRecreationMyrecord>>(){

                }.getType();

                modelRecreationMyrecords= gson.fromJson( response, ModelRecreationMyrecord.class);
            } catch (IOException e){
                e.printStackTrace();
            }

            return null;
        }
    }



    //getting recreation hour starts

    private class GetRecreationHourFromServer extends AsyncTask< Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()){
                pDialog.dismiss();
            }


            btnAddParticipate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(!response_recreation.equals("[]")){
                        Intent intent = new Intent(MyRecordActivity.this, ParticipateNowActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        showAlert.showWarningNoRecreationHour();
                    }

                }
            });


        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                client = new OkHttpClient();
                response_recreation = apiCallRecreationMyRecord.GET(client, path_recreation, account_id);
                Log.e("###GET_RECREATION:", response_recreation);


            } catch (IOException e){
                e.printStackTrace();
            }

            return null;
        }
    }


    //getting recreation hour ends



    public void selectItemDrawer(MenuItem menuItem) {
        Fragment myFragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(MyRecordActivity.this, HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.profile:
                Intent intent1 = new Intent(MyRecordActivity.this, ProfileActivity.class);
                startActivity(intent1);
                break;

            case R.id.log_out_drawer:

                account_id = null;
                SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                editor.putString("account_id", account_id);
                editor.apply();

                Intent intent2 = new Intent(MyRecordActivity.this, LogInActivity.class);
                startActivity(intent2);

                break;

            case R.id.change_password:

                Intent intent3 = new Intent(MyRecordActivity.this, ChangePasswordActivity.class);
                startActivity(intent3);

                break;

            case R.id.mood_meter :
                Intent intent4 = new Intent(MyRecordActivity.this, GetFeelingsActivity.class);
                startActivity(intent4);
                break;

            default:
                // fragmentClass = Network.class;
        }
        try {
//            myFragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.flcontent,myFragment).commit();
        menuItem.setChecked(true);
        //setTitle(menuItem.getTitle());
        dl.closeDrawers();


    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItemDrawer(item);
                return true;
            }
        });
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_record_menu, menu);
        return true;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MyRecordActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
