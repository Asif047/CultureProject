package com.cbc_app_poc.rokomari.rokomarians.Profile;

import android.Manifest;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.ImageUpload.APIUtils;
import com.cbc_app_poc.rokomari.rokomarians.ImageUpload.FileInfo;
import com.cbc_app_poc.rokomari.rokomarians.ImageUpload.FileService;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelTeam;
import com.cbc_app_poc.rokomari.rokomarians.Model.Role;
import com.cbc_app_poc.rokomari.rokomarians.Profile.ApiCalls.ApiCallTeams;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private EditText etFirstName, etLastName, etPhone, etAddress, etHometown, etEducation,
                    etHobbies,  etDesignation, etWork, etJoiningDate;
    private TextView tvChangeImage, tvUploadImage;
    private FloatingActionButton btnSaveProfile;
    private String first_name="", last_name="",image="", phone="", address="", hometown="",
            education="", hobbies="", team="", designation="", work="", joining_date="";

    private Role.User user;
    private Role.UserPersonalInfo userPersonalInfo;
    private Role.UserOfficeInfo userOfficeInfo;

    private EditProfilePostRequest editProfilePostRequest;
    private String account_id="";
    private int id;

    private FileService fileService;
    private String imagePath;
    private Bitmap bitmap;

    private ImageView ivProfile;

    // Storage Permissions starts
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    // Storage Permissions ends

    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private SweetAlertDialog pDialog;

    private Spinner spTeam;
    String[] allTeams;
    ArrayAdapter<String> adapter;
    private String record_team="";


    //getting teams starts
    private String path, response;
    private OkHttpClient client;
    private ModelTeam[] modelTeams;
    private ApiCallTeams apiCallTeams;
    private List<ModelTeam> data;
    //getting team ends

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

//        Toast.makeText(EditProfileActivity.this, allTeams[2], Toast.LENGTH_LONG).show();

        editProfilePostRequest = new EditProfilePostRequest(this);

        //getting account id starts
        SharedPreferences prefs=getSharedPreferences("Profile_PREF",MODE_PRIVATE);
        String restoredAccount=prefs.getString("account_id",null);

        if(restoredAccount!=null)
        {
            account_id=prefs.getString("account_id","No account defined");
        }

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        verifyStoragePermissions(this);

        etFirstName = findViewById(R.id.edittext_first_name_edit_profile);
        etLastName = findViewById(R.id.edittext_last_name_edit_profile);
        etPhone = findViewById(R.id.edittext_phone_edit_profile);
        etAddress = findViewById(R.id.edittext_address_edit_profile);
        etHometown = findViewById(R.id.edittext_hometown_edit_profile);
        etEducation = findViewById(R.id.edittext_education_edit_profile);
        etHobbies = findViewById(R.id.edittext_hobbies_edit_profile);

        etDesignation = findViewById(R.id.edittext_designation_edit_profile);
        etWork = findViewById(R.id.edittext_work_responsibility_edit_profile);
        etJoiningDate = findViewById(R.id.edittext_joining_date_edit_profile);

        tvChangeImage = findViewById(R.id.textview_change_image);
        tvUploadImage = findViewById(R.id.textview_upload_image);

        spTeam = findViewById(R.id.spinner_team_edit_profile);

        ivProfile = findViewById(R.id.image_who);

        etEducation.setInputType(InputType.TYPE_CLASS_TEXT);
        etHobbies.setInputType(InputType.TYPE_CLASS_TEXT);
        etHometown.setInputType(InputType.TYPE_CLASS_TEXT);
        etAddress.setInputType(InputType.TYPE_CLASS_TEXT);
        etWork.setInputType(InputType.TYPE_CLASS_TEXT);


        final Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        first_name = intent.getStringExtra("first_name");
        last_name = intent.getStringExtra("last_name");
        image = intent.getStringExtra("image");
        phone = intent.getStringExtra("phone");
        address = intent.getStringExtra("address");
        hometown = intent.getStringExtra("hometown");
        education = intent.getStringExtra("education");
        hobbies = intent.getStringExtra("hobbies");
        team = intent.getStringExtra("team");
        designation = intent.getStringExtra("designation");
        work = intent.getStringExtra("work");
        joining_date = intent.getStringExtra("joining_date");

        etFirstName.setText(first_name);
        etLastName.setText(last_name);
        etPhone.setText(phone);
        etAddress.setText(address);
        etHometown.setText(hometown);
        etEducation.setText(education);
        etHobbies.setText(hobbies);

        etDesignation.setText(designation);
        etWork.setText(work);
        if(joining_date == null){
            etJoiningDate.setText("");
        } else {
            etJoiningDate.setText(joining_date);
        }

        Picasso.with(this).load(image).into(ivProfile);

        btnSaveProfile = findViewById(R.id.button_save_profile);
        fileService = APIUtils.getFileService();



        //getting team starts
        apiCallTeams = new ApiCallTeams();
        data = new ArrayList<>();


        try{
            path = BaseUrl.BASE_URL_APP + "office/teams";
            new GetDataFromServer().execute();

        } catch (Exception e) {

        }
        //getting team ends




        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userPersonalInfo = new Role.UserPersonalInfo(""+etHometown.getText().toString(),
                        ""+etEducation.getText().toString(),""+etHobbies.getText().toString(),
                        ""+etAddress.getText().toString());

                userOfficeInfo = new Role.UserOfficeInfo(""+record_team,
                        ""+etDesignation.getText().toString(), ""+etWork.getText().toString(),
                        ""+etJoiningDate.getText().toString());

                user = new Role.User(""+etFirstName.getText().toString(), ""+etLastName.getText().toString(),
                        ""+etPhone.getText().toString(),""+image,"ACTIVE",userPersonalInfo,userOfficeInfo);

                if(!myNetworkCheck.isConnected(EditProfileActivity.this)){
                    showAlert.showWarningNetEditProfile();
                } else {
                    editProfilePostRequest.postData(BaseUrl.BASE_URL_APP, user, account_id, id);
                }

            }
        });


        tvChangeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent1 = new Intent(Intent.ACTION_PICK);
                intent1.setType("image/*");
                startActivityForResult(intent1, 0);

                tvUploadImage.setVisibility(View.VISIBLE);
            }
        });


        tvUploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!myNetworkCheck.isConnected(EditProfileActivity.this)){
                    showAlert.showWarningNetEditProfile();
                } else{

                    try{

                        pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                        pDialog.setTitleText("Please wait");
                        pDialog.show();

                        File file = new File(imagePath);
                        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        MultipartBody.Part body = MultipartBody.Part
                                .createFormData("image", file.getName(), requestBody);
                        Log.e("###Image_name:", file.getName());
                        Call<FileInfo> call = fileService.upload(account_id ,body);
                        call.enqueue(new Callback<FileInfo>() {
                            @Override
                            public void onResponse(Call<FileInfo> call, Response<FileInfo> response) {
                                Toast.makeText(EditProfileActivity.this, "Successfully uploaded", Toast.LENGTH_LONG).show();

                                pDialog.dismiss();

                                Intent intent1 = new Intent(EditProfileActivity.this, ProfileActivity.class);
                                startActivity(intent1);
                            }

                            @Override
                            public void onFailure(Call<FileInfo> call, Throwable t) {
                                Toast.makeText(EditProfileActivity.this, "Error: "+t.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        });
                    } catch (Exception e){
                        Toast.makeText(EditProfileActivity.this, "Please Select an Image", Toast.LENGTH_LONG).show();

                    }


                }


            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(data == null){
                Toast.makeText(this, "Unable to choose image !", Toast.LENGTH_SHORT).show();
            } else {

                Uri imageUri = data.getData();
                imagePath = getRealPathFromUri(imageUri);

                try {
                    bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);
                    ivProfile.setImageBitmap(bitmap);

                } catch (IOException e){
                    e.printStackTrace();
                }
            }

        }

    }



    private String getRealPathFromUri(Uri uri){
        String[] projection = { MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri,
                projection, null, null, null);

        Cursor cursor = loader.loadInBackground();
        int column_idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_idx);
        cursor.close();
        return result;
    }



    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }




    //getting team starts
    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e) {

            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if(pDialog.isShowing()) {
                pDialog.dismiss();
            }


            //spinner work starts
            allTeams = new String[data.size()];

            if(team == null) {
                allTeams[0]= "Enter Team";
            } else {
                allTeams[0] = team;
            }

            for(int i = 1; i<data.size(); i++) {

                allTeams[i] = data.get(i).getTeam();
            }

            spTeam = findViewById(R.id.spinner_team_edit_profile);
            adapter = new ArrayAdapter<String>(EditProfileActivity.this, R.layout.spinner_item, allTeams);
            adapter.setDropDownViewResource(R.layout.spinner_item);

            spTeam.setAdapter(adapter);

            spTeam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    record_team =allTeams[position];
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            //spinner work ends



        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                client = new OkHttpClient();
                response = apiCallTeams.GET(client, path, account_id);
                Log.e("##GET_TEAM: ", response);

                Gson gson = new Gson();
                Type type = new TypeToken<Collection<ModelTeam>>() {

                }.getType();

                Collection<ModelTeam> enums = gson.fromJson(response, type);
                modelTeams = enums.toArray(new ModelTeam[enums.size()]);

                if(data.isEmpty()) {
                    for(int i =0; i<enums.size();i++) {
                        data.add(modelTeams[i]);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
    //getting team ends



}
