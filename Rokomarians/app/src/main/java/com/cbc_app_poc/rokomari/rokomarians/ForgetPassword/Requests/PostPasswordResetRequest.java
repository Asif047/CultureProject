package com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.Requests;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.ForgetPassword.ResetPasswordActivity;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostPasswordResetRequest {

    private String responsePost = "";
    private String response_status = "";
    private Context context;


    private ModelPasswordResetResponse modelPasswordResetResponse;

    public PostPasswordResetRequest(Context context) {
        this.context = context;
    }

    public void postData(String resetCode, String url){
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        Map<String, String> params = new HashMap<String, String>();

        params.put("newPassword", resetCode);

        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("content-type", "application/json; charset=utf-8")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                //Log.e("response", call.request().body().toString());

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

//                if (RegisterActivity.pDialog.isShowing())
//                    RegisterActivity.pDialog.dismiss();

                responsePost = response.body().string();

                Gson gson = new Gson();
                Type type = new TypeToken<ModelPasswordResetResponse>() {

                }.getType();

                modelPasswordResetResponse = gson.fromJson(responsePost, type);

                Log.e("###response_code", modelPasswordResetResponse.getMessage());


                if (modelPasswordResetResponse.getMessage().equals("Your password reset successfully")) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Password reset", Toast.LENGTH_SHORT).show();

                        }
                    });

                    Intent intent = new Intent(context, LogInActivity.class);

                    context.startActivity(intent);


                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Password reset failed", Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            }

        });



    }



}
