package com.cbc_app_poc.rokomari.rokomarians.ImageUpload;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;

public class APIUtils {

    private APIUtils(){

    }

    public static final String API_URL= BaseUrl.BASE_URL_APP;
    public static FileService getFileService(){
        return RetrofitClient.getClient(API_URL).create(FileService.class);
    }

}