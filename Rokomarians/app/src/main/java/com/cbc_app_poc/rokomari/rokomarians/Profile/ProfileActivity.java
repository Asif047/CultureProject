package com.cbc_app_poc.rokomari.rokomarians.Profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.AlertBox.ShowAlert;
import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.HomeActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.IdeaBoxActivity;
import com.cbc_app_poc.rokomari.rokomarians.LogIn.LogInActivity;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelProfile;
import com.cbc_app_poc.rokomari.rokomarians.Model.Role;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.cbc_app_poc.rokomari.rokomarians.Utils.MyNetworkCheck;
import com.google.gson.Gson;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;

public class ProfileActivity extends AppCompatActivity {

    private TextView tvFirstname, tvEmail, tvPhone, tvAddress, tvHomeTown, tvEducation, tvHobbies,
            tvTeam, tvDesignation, tvWork, tvJoiningDate;
    private ImageView ivProfile;
    private FloatingActionButton btnEditProfile;

    private String first_name = "", last_name = "", image = "", email_profile = "", phone = "", address = "",
            hometown = "", education = "", hobbies = "", team = "", designation = "",
            work = "", joining_date = "";
    private Integer id = 0;

    private String path, response;
    private OkHttpClient client;
    private ModelProfile modelProfiles;
    private ApiCallProfile apiCallProfile;
    private List<ModelProfile> data;

    private SweetAlertDialog pDialog;
    private MyNetworkCheck myNetworkCheck;
    private ShowAlert showAlert;

    private Toolbar toolbar;
    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    NavigationView nvDrawer;

    private String account_id = null, user_email = "", user_name = "", user_image = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        apiCallProfile = new ApiCallProfile();
        data = new ArrayList<>();
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);

        myNetworkCheck = new MyNetworkCheck();
        showAlert = new ShowAlert(this);

        //getting account id starts
        SharedPreferences prefs = getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredAccount = prefs.getString("account_id", null);

        if (restoredAccount != null) {
            account_id = prefs.getString("account_id", "No account defined");
        }

        String restoredName = prefs.getString("user_name", null);

        if (restoredName != null) {
            user_name = prefs.getString("user_name", "No account defined");
        }

        String restoredEmail = prefs.getString("user_email", null);

        if (restoredEmail != null) {
            user_email = prefs.getString("user_email", "No account defined");
        }

        String restoredImage = prefs.getString("user_image", null);

        if (restoredImage != null) {
            user_image = prefs.getString("user_image", "No account defined");
        }

        //getting account id ends
        
        getSupportActionBar().hide();
        toolbar = (Toolbar) findViewById(R.id.toolbar_profile);
        toolbar.setTitle("Profile");

        // navigation drawer work starts

        dl = (DrawerLayout) findViewById(R.id.dl);
        nvDrawer = (NavigationView) findViewById(R.id.nv);

        toggle = new ActionBarDrawerToggle(this, dl, toolbar, R.string.open, R.string.close);
        dl.addDrawerListener(toggle);

        toggle.syncState();

        getSupportActionBar().setHomeButtonEnabled(true);
        setupDrawerContent(nvDrawer);

        // navigation drawer work ends

        //new starts
        View hView = nvDrawer.getHeaderView(0);
        TextView nav_user_name = (TextView) hView.findViewById(R.id.textview_name_header);
        TextView nav_user_email = (TextView) hView.findViewById(R.id.textview_email_header);
        ImageView nav_user_image = (CircleImageView) hView.findViewById(R.id.image_header);

        nav_user_name.setText(user_name);
        nav_user_email.setText(user_email);
        Picasso.with(this).load(user_image).into(nav_user_image);

        //new ends

        try{
            if(!myNetworkCheck.isConnected(this)){
                showAlert.showWarningNetProfileActivity();
            } else {
                path = BaseUrl.BASE_URL_APP+"users/profile";
                new GetDataFromServer().execute();
            }

        }  catch (Exception e){

        }

        tvFirstname = findViewById(R.id.textview_name_profile);
        tvEmail = findViewById(R.id.textview_email_profile);
        tvPhone = findViewById(R.id.textview_phone_profile);
        tvAddress = findViewById(R.id.textview_address_profile);
        tvHomeTown = findViewById(R.id.textview_hometown_profile);
        tvEducation = findViewById(R.id.textview_education_profile);
        tvHobbies = findViewById(R.id.textview_hobbies_profile);
        tvTeam = findViewById(R.id.textview_team_profile);
        tvDesignation = findViewById(R.id.textview_designation_profile);
        tvWork = findViewById(R.id.textview_work_responsibility_profile);
        tvJoiningDate = findViewById(R.id.textview_joining_date_profile);

        ivProfile = findViewById(R.id.imageview_profile);

        btnEditProfile = findViewById(R.id.button_edit_profile);

        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(ProfileActivity.this, EditProfileActivity.class);

                intent1.putExtra("id", id);
                intent1.putExtra("first_name", first_name);
                intent1.putExtra("last_name", last_name);
                intent1.putExtra("image", image);
                intent1.putExtra("email_profile", email_profile);
                intent1.putExtra("phone", phone);
                intent1.putExtra("address", address);
                intent1.putExtra("hometown", hometown);
                intent1.putExtra("education", education);
                intent1.putExtra("hobbies", hobbies);
                intent1.putExtra("team", team);
                intent1.putExtra("designation", designation);
                intent1.putExtra("work", work);
                intent1.putExtra("joining_date", joining_date);
                startActivity(intent1);
            }
        });

    }


    public void selectItemDrawer(MenuItem menuItem) {
        Fragment myFragment = null;
        Class fragmentClass;
        switch (menuItem.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.profile:
                Intent intent1 = new Intent(ProfileActivity.this, ProfileActivity.class);
                startActivity(intent1);
                break;

            case R.id.log_out_drawer:

                account_id = null;
                SharedPreferences.Editor editor = getSharedPreferences("Profile_PREF", MODE_PRIVATE).edit();
                editor.putString("account_id", account_id);
                editor.apply();

                Intent intent2 = new Intent(ProfileActivity.this, LogInActivity.class);
                startActivity(intent2);

                break;

            default:

        }

        menuItem.setChecked(true);
        //setTitle(menuItem.getTitle());
        dl.closeDrawers();


    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItemDrawer(item);
                return true;
            }
        });
    }


    private class GetDataFromServer extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try{
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#26A65B"));
                pDialog.setTitleText("Loading");
                pDialog.setCancelable(false);
                pDialog.show();
            } catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(pDialog.isShowing()){
                pDialog.dismiss();
            }


            try {
                id = modelProfiles.getId();
                first_name = modelProfiles.getFirstName();
                last_name = modelProfiles.getLastName();
                image = modelProfiles.getImage().getImageUrl();
                email_profile = modelProfiles.getEmail();
                phone = modelProfiles.getPhone();
                address = modelProfiles.getUserPersonalInfo().getAddress();
                hometown = modelProfiles.getUserPersonalInfo().getHomeTown();
                education = modelProfiles.getUserPersonalInfo().getEducation();
                hobbies = modelProfiles.getUserPersonalInfo().getHobbies();
                team = modelProfiles.getUserOfficeInfo().getTeam();
                designation = modelProfiles.getUserOfficeInfo().getDesignation();
                work = modelProfiles.getUserOfficeInfo().getWork();
                joining_date = modelProfiles.getUserOfficeInfo().getJoiningDate();
            } catch (Exception e){
                showAlert.showWarningNeedRegistration();
            }

           // Toast.makeText(ProfileActivity.this, "" + id, Toast.LENGTH_LONG).show();


            tvFirstname.setText(first_name);
            tvEmail.setText(email_profile);
            tvPhone.setText(phone);
            tvAddress.setText(address);
            tvHomeTown.setText(hometown);
            tvEducation.setText(education);
            tvHobbies.setText(hobbies);
            tvTeam.setText(team);
            tvDesignation.setText(designation);
            tvWork.setText(work);
            tvJoiningDate.setText(joining_date);

            Picasso.with(ProfileActivity.this).load(image).into(ivProfile);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                client = new OkHttpClient();
                response = apiCallProfile.GET(client, path, account_id);
                Log.e("###GET_PROFILE:", response);
                Gson gson = new Gson();

                modelProfiles = gson.fromJson(response, ModelProfile.class);
            } catch (IOException e){
                e.printStackTrace();
            }

            return null;

        }
    }


    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
        startActivity(intent);
    }
}
