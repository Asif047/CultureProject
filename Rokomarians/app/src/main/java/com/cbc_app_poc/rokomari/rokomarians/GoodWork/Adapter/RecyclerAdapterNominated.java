package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Adapter;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.BaseUrl;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.EditGoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request.DeleteGoodWorkRequest;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request.PostLikeGoodWork;
import com.cbc_app_poc.rokomari.rokomarians.Interfaces.ItemClickListener;
import com.cbc_app_poc.rokomari.rokomarians.Model.ModelNominated;
import com.cbc_app_poc.rokomari.rokomarians.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class RecyclerAdapterNominated extends RecyclerView.Adapter<RecyclerAdapterNominated.MyViewHolder> {

    private List<ModelNominated> modelNominateds;
    private Context context;
    private String id = "";
    private String status = "";
    private String account_id = "", user_id="";
    private PostLikeGoodWork postLikeGoodWork;

    private DeleteGoodWorkRequest deleteGoodWorkRequest;

    public RecyclerAdapterNominated( Context context, List<ModelNominated> modelNominateds, String account_id) {
        this.modelNominateds = modelNominateds;
        this.context = context;
        this.account_id = account_id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.row_item_good_work, parent, false);

        postLikeGoodWork = new PostLikeGoodWork(context);

        deleteGoodWorkRequest = new DeleteGoodWorkRequest(context);


        //getting user id starts
        SharedPreferences prefs = context.getSharedPreferences("Profile_PREF", MODE_PRIVATE);
        String restoredUserId = prefs.getString("user_id", null);

        if (restoredUserId != null) {
            user_id = prefs.getString("user_id", "No user id defined");
        }

        //getting user id ends


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvNameWork.setText(modelNominateds.get(position).getUserFirstName());
        holder.tvNominatedname.setText(modelNominateds.get(position).getNominatedUserFirstname());
        holder.tvDetailsWork.setText(modelNominateds.get(position).getReason());
        holder.tvDateWork.setText(modelNominateds.get(position).getNominationDate());
        holder.tvNumOfLikes.setText(""+modelNominateds.get(position).getNumberOfLikes());


        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(user_id.equals(modelNominateds.get(position).getNominatedByUserID())) {
                    //Toast.makeText(context, "Wait", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, EditGoodWorkActivity.class);
                    intent.putExtra("nominated_id", modelNominateds.get(position).getId());
                    intent.putExtra("nominated_name", modelNominateds.get(position).getNominatedUserFirstname());
                    intent.putExtra("nominated_image", modelNominateds.get(position).getNominatedUserImage());
                    intent.putExtra("nominated_reason", modelNominateds.get(position).getReason());

                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "You can't edit other's post", Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(user_id.equals(modelNominateds.get(position).getNominatedByUserID())) {
                   // Toast.makeText(context, "Wait", Toast.LENGTH_SHORT).show();
                    deleteGoodWorkRequest.deleteData(modelNominateds.get(position).getId(),
                            account_id, BaseUrl.BASE_URL_APP+"nomination/");

                } else {
                    Toast.makeText(context, "You can't Delete other's post", Toast.LENGTH_SHORT).show();
                }



            }
        });


        if(modelNominateds.get(position).getLiked()==true){
            holder.ivLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like_blue));
        } else {
            holder.ivLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_good_work));
        }

        Picasso.with(context).load(modelNominateds.get(position).getUserImage()).into(holder.ivRokomarian);
        Picasso.with(context).load(modelNominateds.get(position).getNominatedUserImage()).into(holder.ivNominated);

        holder.ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postLikeGoodWork.postData(BaseUrl.BASE_URL_APP,
                        ""+modelNominateds.get(position).getId(), account_id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelNominateds.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemClickListener itemClickListener;
        ImageView ivRokomarian, ivNominated, ivLike, ivEdit, ivDelete;
        TextView tvNameWork, tvDateWork, tvDetailsWork, tvNominated, tvNominatedname, tvNumOfLikes;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivRokomarian = itemView.findViewById(R.id.imageview_rokomarian_work);
            ivNominated = itemView.findViewById(R.id.imageview_nominated);
            ivLike = itemView.findViewById(R.id.imageview_like_work);

            ivEdit = itemView.findViewById(R.id.image_edit);
            ivDelete = itemView.findViewById(R.id.image_delete);

            tvNameWork = itemView.findViewById(R.id.textview_name_work);
            tvDateWork = itemView.findViewById(R.id.textview_date_work);
            tvDetailsWork = itemView.findViewById(R.id.textview_details_work);
            tvNominated = itemView.findViewById(R.id.textview_nominated);
            tvNominatedname = itemView.findViewById(R.id.textview_nominated_name);
            tvNumOfLikes = itemView.findViewById(R.id.textview_num_like_work);
        }


        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }
    }

}
