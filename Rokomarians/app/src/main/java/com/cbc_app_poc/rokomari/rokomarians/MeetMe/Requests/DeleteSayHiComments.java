package com.cbc_app_poc.rokomari.rokomarians.MeetMe.Requests;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request.ModelDeleteGoodWork;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DeleteSayHiComments {

    private String responsePost = "";
    private String response_status = "";
    private Context context;

    private ModelDeleteGoodWork modelDeleteGoodWork;

    public DeleteSayHiComments(Context context) {
        this.context = context;
    }

    public void deleteData(String commentId, String account_id, String url){

        MediaType JSON = MediaType.parse("application/json; charset= utf-8");
        Map<String, String> params = new HashMap<String, String>();

        params.put("Id", commentId);

        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());
        Request request = new Request.Builder()
                .url(url)
                .delete(body)
                .header("Authorization", account_id)
                .header("commentId",commentId)
                .addHeader("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                responsePost = response.body().string();
                Log.e("###response_code:", responsePost);

                Gson gson= new Gson();
                Type type = new TypeToken<ModelDeleteGoodWork>() {

                }.getType();

                modelDeleteGoodWork = gson.fromJson(responsePost, type);
                Log.e("#RESPONSE_DEL_SAY_HI:", modelDeleteGoodWork.getMessage().toString());

                response_status = modelDeleteGoodWork.getMessage();

                if(response_status.equals("Comment deleted.")) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                        }
                    });

                    ((Activity)context).finish();
                    context.startActivity(((Activity)context).getIntent());

                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Delete failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }



            }
        });

    }


}
