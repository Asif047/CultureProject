package com.cbc_app_poc.rokomari.rokomarians.GoodWork.Request;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.cbc_app_poc.rokomari.rokomarians.GoodWork.Activity.GoodWorkActivity;
import com.cbc_app_poc.rokomari.rokomarians.IdeaBox.Activities.IdeaBoxActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DeleteGoodWorkRequest {


    private String responsePost = "";
    private String response_status = "";
    private Context context;

    private ModelDeleteGoodWork modelDeleteGoodWork;

    public DeleteGoodWorkRequest(Context context) {
        this.context = context;
    }

    public void deleteData(String elementId, String account_id, String url){

        MediaType JSON = MediaType.parse("application/json; charset= utf-8");
        Map<String, String> params = new HashMap<String, String>();

        params.put("Id", elementId);

        JSONObject parameter = new JSONObject(params);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, parameter.toString());
        Request request = new Request.Builder()
                .url(url)
                .delete(body)
                .header("Authorization", account_id)
                .header("id",elementId)
                .addHeader("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                responsePost = response.body().string();
                Log.e("###response_code:", responsePost);

                Gson gson= new Gson();
                Type type = new TypeToken<ModelDeleteGoodWork>() {

                }.getType();

                modelDeleteGoodWork = gson.fromJson(responsePost, type);
                Log.e("#RESPONSE_DEL_NOMINEE:", modelDeleteGoodWork.getMessage().toString());

                response_status = modelDeleteGoodWork.getMessage();

                if(response_status.equals("Deleted.")) {

                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                        }
                    });

                    Intent intent = new Intent(context, GoodWorkActivity.class);
                    context.startActivity(intent);
                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "Delete failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }



            }
        });

    }


}
